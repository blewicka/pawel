<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
//use SEO;

Auth::routes();
Route::post('send-mail/', function (Request $request) {
    
        
        $data = $request->validate([
            'contact_name' => 'required',
            'contact_message' => 'required',
            'contact_phone'=>'',
            'contact_email' =>''
        ]);
    
        Mail::raw('Imię i nazwisko: '.$data['contact_name']."\n".'Numer telefonu:'.$data['contact_phone']."\n".'Adres e-mail:'.$data['contact_email']."\n".$data['contact_message'], function ($message) {
            $contact_mail = App\Contact::where('lang', '=', 'pl')->where('property', '=', 'contact_mail')->first();
            //$message->to($contact_mail->value);
            //$message->from('dragonn@op.pl');
        $message->to($contact_mail->value); 
        $message->from($contact_mail->value);
            $message->subject('Wiadomość ze strony');
        });
    
    });
    

Route::get('/logout/', 'Auth\LoginController@logout');

Route::get('/admin/{lang?}/', function ($lang='pl') {
    
    return redirect('/admin/biography/pl\/');

})->middleware('auth');

Route::get('/{lang?}/', function (Request $request, $lang=null) {

    //osbługa języka z przeglądarki
    if ($lang==null){
        $lang = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);   
        if ($lang!='pl' && $lang!='en'){
            $lang='en';
        }
    }

    if ($lang!='pl' && $lang!='en'){
        $lang='en';
    }
       
    $links = \App\Link::where('id', 1)->get(); //pobiera wszystkie linki z bazy danych
    //return view('welcome');
    App::setLocale($lang);
    $music_list = App\Music::where('lang', '=', $lang)->orderBy('position','asc')->get();
    $video_list = App\Youtube::where('lang', '=', $lang)->orderBy('position','asc')->get();
    $media_video_list = App\MediaYoutube::where('lang', '=', $lang)->orderBy('position','asc')->get();
    $media_articles_list = App\MediaArticle::where('lang', '=', $lang)->orderBy('position','asc')->get();
    $media_recordings_list = App\MediaMusic::where('lang', '=', $lang)->orderBy('position','asc')->get();
    $biography = App\Biography::where('lang', '=', $lang)->first();
    $photos_list = App\Photo::where('lang', '=', $lang)->orderBy('position','asc')->get();
    $concerts_list = App\Concert::where('lang', '=', $lang)->orderBy('date','desc')->orderBy('time','desc')->get();
    $concerts_years = App\Concert::where('lang', '=', $lang)->groupBy(DB::raw('YEAR(date)'))->orderBy('date','desc')->get();
    
    SEOMeta::addAlternateLanguage('en', 'pawelalbinski.com/en');
    SEOMeta::addAlternateLanguage('pl', 'pawelalbinski.pl/pl');


    $page_title = App\Seo::where('lang', '=', $lang)->where('property', '=', 'page_title')->first();
    $page_title_value=$page_title['value'];
    $page_tags = App\Seo::where('lang', '=', $lang)->where('property', '=', 'page_tags')->first();
    $page_tags_value=$page_tags['value'];
    $page_description = App\Seo::where('lang', '=', $lang)->where('property', '=', 'page_description')->first();
    $page_description_value=$page_description['value'];

    SEO::setTitle($page_title_value);
    SEO::setDescription($page_description_value);
    SEOMeta::setKeywords($page_tags_value);

    $contact_content = App\Contact::where('lang', '=', $lang)->where('property', '=', 'contact_content')->first();
   

    return view('main', ['biography_text'=>$biography->text,
                        'video_list'=>$video_list,
                        'music_list'=>$music_list,
                        'media_video_list'=>$media_video_list,
                        'media_articles_list'=>$media_articles_list,
                        'media_recordings_list'=>$media_recordings_list,
                        'photos_list'=>$photos_list,
                        'concerts_years'=>$concerts_years,
                        'concerts_list'=>$concerts_list,
                        'lang'=>$lang,
                        'contact_content'=>$contact_content]);
    //return 'wtf';
    // return view('welcome')->withLinks($links); //assign->withlinks
    
});


Route::get('/submit/', function () {
    return view('submit');
});

//Biografia

Route::get('/admin/biography/{lang?}/', function ($lang='pl') {
    $biography = App\Biography::where('lang', '=', $lang)->first();
    return view('/admin/admin-biography', ['biography_text'=>$biography->text,
                                          'curr_lang'=>$lang]);

})->middleware('auth');;

Route::post('/admin/biography/{lang?}/save/', function (Request $request, $lang='pl') {
    $data = $request->validate([
        'text' => 'required'
    ]);

    $biography = App\Biography::where('lang', '=', $lang)->first();
    $biography->text = $data['text'];
    $biography->save();  

   
})->middleware('auth');;



// MUZYKA -songs

Route::get('/admin/music/songs/{lang?}/', function ($lang='pl') {
    $music_list = App\Music::where('lang', '=', $lang)->orderBy('position','asc')->get();
    return view('/admin/admin-music-songs', ['curr_lang'=>$lang,
                                            'music_list'=>$music_list]);
})->middleware('auth');;

Route::post('/admin/music/songs/{lang?}/save/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'song_title' => 'required'
    ]);

    $music = new App\Music();
    $music->name = $data['song_title'];
    $music->lang = $lang;
    $music->save();
    
    $path = $request->file('song')->storeAs('public/music/songs/', $music->id.'.mp3');
    
    //wyswietlenie po dodaniu
    return response()->json([ 'new_id' => $music->id]);
   
})->middleware('auth');

Route::post('/admin/music/songs/delete/', function (Request $request) {
    
    $data = $request->validate([
        'id_to_remove' => 'required'
    ]);

    $id_to_remove=$data['id_to_remove'];
    //File::delete('public/music/songs'.$id_to_remove.'.mp3');
    Storage::delete('public/music/songs/'.$id_to_remove.'.mp3');
    

    $title_to_delete = App\Music::where('id', '=', $id_to_remove)->first();
    $title_to_delete->delete();
       
})->middleware('auth');;

Route::post('/admin/music/songs/{lang?}/sort-recordings/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'sorted_ids' => 'required'
    ]);

    foreach ($data['sorted_ids'] as $id_key=>$id_value){
        $recording= App\Music::where('id', '=', $id_value)->first();
        $recording->position=$id_key;
        $recording->save();
    }
    
})->middleware('auth');;

// MUZYKA - videos
Route::get('/admin/music/videos/{lang?}/', function ($lang='pl') {
    $video_list = App\Youtube::where('lang','=',$lang)->orderBy('position','asc')->get();
    return view('/admin/admin-music-videos', ['curr_lang'=>$lang,
                                              'video_list'=>$video_list]);
})->middleware('auth');;

Route::post('/admin/music/videos/{lang?}/save/', function (Request $request, $lang='pl') {
   
    $data = $request->validate([
        'video_title' => 'required',
        'video_link_id' => 'required'
    ]);

    $youtube_link = new App\Youtube();
    $youtube_link->title = $data['video_title'];
    $youtube_link->video_id = $data['video_link_id'];
    $youtube_link->lang=$lang;
    $youtube_link->save();  
    
    return response()->json([ 'new_id' => $youtube_link->id]);
   
})->middleware('auth');;

Route::post('/admin/music/videos/delete/', function (Request $request) {
    
    $data = $request->validate([
        'id_to_remove' => 'required'
    ]);

    $id_to_remove=$data['id_to_remove']; 
    
    $youtube_link_delete = App\Youtube::where('id', '=', $id_to_remove)->first();
    $youtube_link_delete->delete();
       
})->middleware('auth');;

Route::post('/admin/music/videos/{lang?}/sort-videos/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'sorted_ids' => 'required'
    ]);

    foreach ($data['sorted_ids'] as $id_key=>$id_value){
        $videos= App\Youtube::where('id', '=', $id_value)->first();
        $videos->position=$id_key;
        $videos->save();
    }
    
})->middleware('auth');;



//MEDIA

Route::get('/admin/media/articles/{lang?}/', function ($lang='pl') {
    $media_articles_list = App\MediaArticle::where('lang', '=', $lang)->orderBy('position','asc')->get();
    return view('/admin/admin-media-articles', ['curr_lang'=>$lang,
                                               'media_articles_list'=>$media_articles_list]);
})->middleware('auth');;

    
Route::post('/admin/media/articles/{lang?}/save/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'article_title' => 'required'
    ]);

    $media_article = new App\MediaArticle();
    $media_article->name = $data['article_title'];
    $media_article->lang = $lang;
    $media_article->save();
    
    $path = $request->file('article')->storeAs('public/media/articles/', $media_article->id.'.jpg');
    
    $imgblur = Image::make('storage/media/articles/'. $media_article->id.'.jpg');
    $imgblur->interlace(true);
    $imgblur->widen(100);
    $imgblur->save(public_path('storage/media/articles/'. $media_article->id.'-blur.jpg'));


    $imgorg = Image::make('storage/media/articles/'. $media_article->id.'.jpg');
    $imgorg->interlace(true);
    //$imgorg->widen(100);
    $imgorg->save(public_path('storage/media/articles/'. $media_article->id.'.jpg'));
    
    return response()->json([ 'new_id' => $media_article->id]);
    
})->middleware('auth');

Route::post('/admin/media/articles/delete/', function (Request $request) {
    
    $data = $request->validate([
        'id_to_remove' => 'required'
    ]);

    $id_to_remove=$data['id_to_remove'];
    Storage::delete('public/media/articles/'.$id_to_remove.'.jpg');
    

    $title_to_delete = App\MediaArticle::where('id', '=', $id_to_remove)->first();
    $title_to_delete->delete();
        
})->middleware('auth');;

Route::post('/admin/media/articles/{lang?}/sort-articles/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'sorted_ids' => 'required'
    ]);

    foreach ($data['sorted_ids'] as $id_key=>$id_value){
        $article= App\MediaArticle::where('id', '=', $id_value)->first();
        $article->position=$id_key;
        $article->save();
    }
    
})->middleware('auth');;

//MEDIA songs
Route::get('/admin/media/recordings/{lang?}/', function ($lang='pl') {

    $media_recordings_list = App\MediaMusic::where('lang', '=', $lang)->orderBy('position','asc')->get();
    return view('/admin/admin-media-songs', ['curr_lang'=>$lang,
                                            'media_recordings_list'=>$media_recordings_list]);
})->middleware('auth');;

Route::post('/admin/media/recordings/{lang?}/save/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'song_title' => 'required'
    ]);

    $media_music = new App\MediaMusic();
    $media_music->name = $data['song_title'];
    $media_music->lang = $lang;
    $media_music->save();
    
    $path = $request->file('song')->storeAs('public/media/songs/', $media_music->id.'.mp3');
    
    return response()->json([ 'new_id' => $media_music->id]);
   
})->middleware('auth');;

Route::post('/admin/media/recordings/delete/', function (Request $request) {
    
    $data = $request->validate([
        'id_to_remove' => 'required'
    ]);

    $id_to_remove=$data['id_to_remove'];
    Storage::delete('public/media/songs/'.$id_to_remove.'.mp3');
    

    $title_to_delete = App\MediaMusic::where('id', '=', $id_to_remove)->first();
    $title_to_delete->delete();
       
})->middleware('auth');;

Route::post('/admin/media/recordings/{lang?}/sort-recordings/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'sorted_ids' => 'required'
    ]);

    foreach ($data['sorted_ids'] as $id_key=>$id_value){
        $recording= App\MediaMusic::where('id', '=', $id_value)->first();
        $recording->position=$id_key;
        $recording->save();
    }
    
})->middleware('auth');;

//MEDIA video
Route::get('/admin/media/videos/{lang?}/', function ($lang='pl') {
    $media_video_list = App\MediaYoutube::where('lang','=',$lang)->orderBy('position','asc')->get();
    return view('/admin/admin-media-videos', ['curr_lang'=>$lang,
                                              'media_video_list'=>$media_video_list]);
})->middleware('auth');;

Route::post('/admin/media/videos/{lang?}/save/', function (Request $request, $lang='pl') {
    
     $data = $request->validate([
         'video_title' => 'required',
         'video_link_id' => 'required'
     ]);
 
     $media_youtube_link = new App\MediaYoutube();
     $media_youtube_link->title = $data['video_title'];
     $media_youtube_link->video_id = $data['video_link_id'];
     $media_youtube_link->lang=$lang;
     $media_youtube_link->save();  
     
     return response()->json([ 'new_id' => $media_youtube_link->id]);
    
 })->middleware('auth');;
 
 Route::post('/admin/media/videos/delete/', function (Request $request) {
     
     $data = $request->validate([
         'id_to_remove' => 'required'
     ]);
 
     $id_to_remove=$data['id_to_remove']; 
     
     $media_youtube_link_delete = App\MediaYoutube::where('id', '=', $id_to_remove)->first();
     $media_youtube_link_delete->delete();
        
 })->middleware('auth');;

 Route::post('/admin/media/videos/{lang?}/sort-videos/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'sorted_ids' => 'required'
    ]);

    foreach ($data['sorted_ids'] as $id_key=>$id_value){
        $videos= App\MediaYoutube::where('id', '=', $id_value)->first();
        $videos->position=$id_key;
        $videos->save();
    }
    
})->middleware('auth');;

//GALERIA

Route::get('/admin/gallery/{lang?}/', function ($lang='pl') {
    $photos_list = App\Photo::where('lang', '=', $lang)->orderBy('position','asc')->get();
    return view('/admin/admin-gallery', ['curr_lang'=>$lang,
                                         'photos_list'=>$photos_list]);
})->middleware('auth');;

    
Route::post('/admin/gallery/{lang?}/save/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'photo_title' => 'required'
    ]);

    $photo = new App\Photo();
    $photo->name = $data['photo_title'];
    $photo->lang = $lang;
    $photo->save();
    
    $request->file('photo')->storeAs('public/gallery/', $photo->id.'.jpg');
    
    $request->file('thumb')->storeAs('public/gallery/', $photo->id.'-thumb.jpg');
    //print_r('storage/'. $photo->id.'-thumb.jpg');
    //die();
    $imgthumb = Image::make('storage/gallery/'. $photo->id.'-thumb.jpg');
    $imgthumb->interlace(true);
    $imgthumb->resize(400, 225);
    $imgthumb->save(public_path('storage/gallery/'. $photo->id.'-thumb.jpg'));
    
    $imgblur = Image::make('storage/gallery/'. $photo->id.'.jpg');
    $imgblur->interlace(true);
    $imgblur->widen(100);
    $imgblur->save(public_path('storage/gallery/'. $photo->id.'-blur.jpg'));


    $imgorg = Image::make('storage/gallery/'. $photo->id.'.jpg');
    $imgorg->interlace(true);
    //$imgorg->widen(100);
    $imgorg->save(public_path('storage/gallery/'. $photo->id.'.jpg'));
    return response()->json([ 'new_id' => $photo->id]);
    
})->middleware('auth');;

Route::post('/admin/gallery/delete/', function (Request $request) {
    
    $data = $request->validate([
        'id_to_remove' => 'required'
    ]);

    $id_to_remove=$data['id_to_remove'];
    Storage::delete('public/gallery/'.$id_to_remove.'.jpg');
    

    $title_to_delete = App\Photo::where('id', '=', $id_to_remove)->first();
    $title_to_delete->delete();
        
})->middleware('auth');;

Route::post('/admin/gallery/{lang?}/sort-photos/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'sorted_ids' => 'required'
    ]);

    foreach ($data['sorted_ids'] as $id_key=>$id_value){
        $photos= App\Photo::where('id', '=', $id_value)->first();
        $photos->position=$id_key;
        $photos->save();
    }
    
})->middleware('auth');;


//KONCERTY

Route::get('/admin/concerts/{lang?}/', function ($lang='pl') {
    $concerts_list = App\Concert::where('lang', '=', $lang)->orderBy('date','desc')->orderBy('time','desc')->get();

    return view('/admin/admin-concerts', ['curr_lang'=>$lang,
                                          'concerts_list'=>$concerts_list]);
})->middleware('auth');;


Route::post('/admin/concerts/{lang?}/save/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'concert_location' => 'required',
        'concert_descripiton' => 'required',
        'concert_date' => 'required',
        'concert_time' => 'required'
    ]);

    $concert = new App\Concert();
    $concert->lang = $lang;
    $concert->location = $data['concert_location'];
    $concert->description = $data['concert_descripiton'];
    $concert->date = $data['concert_date'];
    $concert->time = $data['concert_time'];
    $concert->save();
    
    $request->file('poster')->storeAs('public/concerts/', $concert->id.'.jpg');   
    return response()->json([ 'new_id' => $concert->id]);
    
})->middleware('auth');;


Route::post('/admin/concerts/{lang?}/edit/', function (Request $request, $lang='pl') {
    
    $data = $request->validate([
        'concert_location' => 'required',
        'concert_descripiton' => 'required',
        'concert_date' => 'required',
        'concert_time' => 'required',
        'concert_id' => 'required',

    ]);

    $concert = App\Concert::where('id', '=', $data['concert_id'])->first();
    $concert->lang = $lang;
    $concert->location = $data['concert_location'];
    $concert->description = $data['concert_descripiton'];
    $concert->date = $data['concert_date'];
    $concert->time = $data['concert_time'];
    $concert->save();
    if ($request->hasFile('poster')) {
        $request->file('poster')->storeAs('public/concerts/', $concert->id.'.jpg');   
    }
    return response()->json([ 'new_id' => $concert->id]);
    
})->middleware('auth');;

Route::post('/admin/concerts/delete/', function (Request $request) {
    
    $data = $request->validate([
        'id_to_remove' => 'required'
    ]);

    $id_to_remove=$data['id_to_remove'];
    Storage::delete('public/concerts/'.$id_to_remove.'.jpg');
    

    $title_to_delete = App\Concert::where('id', '=', $id_to_remove)->first();
    $title_to_delete->delete();
        
})->middleware('auth');;


//USTAWIENIA

Route::get('/admin/settings/{lang?}/', function ($lang='pl') {
    return view('/admin/admin-settings', ['curr_lang'=>$lang]);
})->middleware('auth');

Route::post('/admin/settings/save/', function (Request $request) {
    $data = $request->validate([
        'title' => 'required'
    ]);

    $request->file('photo')->storeAs('public/border-photos/', $data['title'].'.jpg');
})->middleware('auth');;

//SEO
Route::get('/admin/seo/{lang?}/', function ($lang='pl') {
    
    $page_title = App\Seo::where('lang', '=', $lang)->where('property', '=', 'page_title')->first();
    $page_tags = App\Seo::where('lang', '=', $lang)->where('property', '=', 'page_tags')->first();
    $page_description = App\Seo::where('lang', '=', $lang)->where('property', '=', 'page_description')->first();

    return view('/admin/admin-seo', ['curr_lang'=>$lang,
                                    'page_title'=>$page_title,
                                    'page_tags'=>$page_tags,
                                    'page_description'=>$page_description]);
})->middleware('auth');;

Route::post('/admin/seo/{lang?}/save-seo/', function ($lang='pl',Request $request) {

    $data = $request->validate([
        'page_title' => '',
        'page_tags' =>'',
        'page_description'=>''
    ]);

    $page_title = App\Seo::where('lang', '=', $lang)->where('property', '=', 'page_title')->first();
    $page_title->value = $data['page_title'];
    $page_title->save();  

    $page_tags = App\Seo::where('lang', '=', $lang)->where('property', '=', 'page_tags')->first();
    $page_tags->value = $data['page_tags'];
    $page_tags->save();

    $page_description = App\Seo::where('lang', '=', $lang)->where('property', '=', 'page_description')->first();
    $page_description->value = $data['page_description'];
    $page_description->save();

})->middleware('auth');;

//KONTAKT
Route::get('/admin/contact/{lang?}/', function ($lang='pl') {
    $contact_content = App\Contact::where('lang', '=', $lang)->where('property', '=', 'contact_content')->first();
    $contact_mail = App\Contact::where('lang', '=', 'pl')->where('property', '=', 'contact_mail')->first();
    return view('/admin/admin-contact', ['curr_lang'=>$lang,
                                         'contact_mail'=>$contact_mail,
                                         'contact_content'=>$contact_content
                                         ]);
})->middleware('auth');;

Route::post('/admin/contact/{lang?}/content-save/', function ($lang='pl',Request $request) {

    $data = $request->validate([
        'text' => '',
    ]);

    $contact_content = App\Contact::where('lang', '=', $lang)->where('property', '=', 'contact_content')->first();
    $contact_content->value = $data['text'];
    $contact_content->save();  

})->middleware('auth');;

Route::post('/admin/contact/mail-save/', function ($lang='pl',Request $request) {

    $data = $request->validate([
        'contact_mail' => 'required',
    ]);

    $contact_mail = App\Contact::where('lang', '=', 'pl')->where('property', '=', 'contact_mail')->first();
    $contact_mail->value = $data['contact_mail'];
    $contact_mail->save();  

})->middleware('auth');;

// Route::post('/submit/', function (Request $request) {
//     $data = $request->validate([
//         'title' => 'required|max:255',
//         'url' => 'required|url|max:255',
//         'description' => 'required|max:255',
//     ]);

//     $link = tap(new App\Link($data))->save();

//     return redirect('/');   
// });






// Route::get('/home', 'HomeController@index')->name('home');


?>

