/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 56);
/******/ })
/************************************************************************/
/******/ ({

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

    module.exports = __webpack_require__(57);
    
    
    /***/ }),
    
    /***/ 57:
    /***/ (function(module, exports) {
    
    function delete_video() {
        $('.delete-video').click(function () {
    
            var video_id = $(this).parent().parent().attr("id");
            //console.log($(this).closest('div').attr('id'));
    
            var th = this;
            $.post('/admin/media/videos/delete', { id_to_remove: video_id }).done(function (data) {
                $(th).parent().parent().remove();
            });
        });
    }
    
    $(document).ready(function () {
    
        $('#add-video').click(function () {
    
            var v_title = $('#video-title').val();
            var v_link_id = $('#video-link-id').val();
    
            if (v_title == '' || v_link_id == '') {
                alert("Wpisz tytuł i id linku");
                return;
            }
    
            $("#add-video").prop("disabled", true);
    
            $.post('save', { video_title: v_title, video_link_id: v_link_id }, function (data) {
    
                $('#video-title').val('');
                $('#video-link-id').val('');
    
                console.log(data.new_id);
                $('#admin-video-table').find('tbody').append('                   \n               <tr id="' + data.new_id + '">   \n                    <td>\n                       ' + v_title + ' &nbsp;&nbsp;\n                    </td>\n                \n\n                    <td>\n\n                    <iframe width="280" height="120" src="https://www.youtube.com/embed/' + v_link_id + '" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>\n                        \n                    </td>\n\n                    <td>\n                        <button class="btn btn-secondary delete-video" name="delete-video-button"><i class="fa fa-times"></i></button>\n                    </td>\n               </tr>          \n            ');
    
                $("#add-video").prop("disabled", false);
    
                delete_video();
            });
        });
    
        delete_video();
    });
    
    /***/ })
    
    /******/ });