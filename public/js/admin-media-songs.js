/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 58);
/******/ })
/************************************************************************/
/******/ ({

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

    module.exports = __webpack_require__(59);
    
    
    /***/ }),
    
    /***/ 59:
    /***/ (function(module, exports) {
    
    function delete_song() {
        $('.delete-song').click(function () {
    
            var song_id = $(this).parent().parent().attr("id");
            //console.log($(this).closest('div').attr('id'));
    
            var th = this;
            $.post('/admin/media/songs/delete', { id_to_remove: song_id }).done(function (data) {
                $(th).parent().parent().remove();
            });
        });
    }
    
    function progress(e) {
        //e - akcja która się zdarza w trakcie wysyłania co kawałek
        if (e.lengthComputable) {
            //info czy da się policzyć wysyłanie
            var max = e.total; //ile jest cały
            var current = e.loaded; // ile już się wysłało
            var Percentage = current * 100 / max;
            console.log(Percentage);
            $(".progress-bar").attr('aria-valuenow', Percentage.toFixed(0));
            $(".progress-bar").attr('style', "width:" + Percentage.toFixed(0) + "%");
            $("#upload-progress").text(Percentage.toFixed(0) + " %"); //ustawia ten procent jako tekst w jakimś elemencie
            if (Percentage >= 100) {}
        }
    }
    
    $(document).ready(function () {
    
        //obsługa zmiany napisu po dodaniu pliku do zapisania
        $('#song-file').on('change', function () {
            // console.log('zmiana pliku');
            $("#upload-song-button").text($('#song-file')[0].files[0].name);
        });
    
        //dodawanie pliku
        $('#add-song').click(function () {
    
            //console.log('funkcja wysylania');
            var fd = new FormData(); //tworzy nową formę na dane
            var song_file = $('#song-file')[0].files[0]; //wybrany plik
            fd.append('song', song_file); //dodaję plik do formy 'x'-nazwa, y - wartosc:liczba, text, plik itd
    
            var song_title = $('#song-title').val();
            fd.append('song_title', song_title);
            console.log(song_file);
            //console.log(song_title);
    
            if (song_title == '' || song_file == null) {
                alert("Wpisz tytuł i treść");
                return;
            }
    
            $("#add-song").prop("disabled", true);
            $("#song-progress-bar").removeClass("hide");
            $.ajax({ //wysyłam plik
                url: "save", //adres pod który chcę go wysłać
                type: "POST", //metoda którą chcę go wysłać
                data: fd, //dane które chcę wysłać czyli nasza forma z danymi
                enctype: 'multipart/form-data', //typ danych
                processData: false, // tell jQuery not to process the data
                contentType: false, // tell jQuery not to set contentType
                xhr: function xhr() {
                    //nie ważne, tutaj można dorobić śledzenie progresu wrzucania
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', progress, false);
                    }
                    return myXhr;
                }
            }).done(function (data) {
                //funkcja wykonywana po zakeńczeniu wysyłania
    
                $('#song-title').val('');
                $('#song-file').val('');
                $("#upload-song-button").html('<i class="fa fa-paperclip"></i>');
    
                $('#admin-songs-table').find('tbody').append('\n                \n                \n                   <tr id="' + data.new_id + '">\n                        <td>\n                        ' + song_title + ' &nbsp;&nbsp;\n                        </td>\n\n                        <td>\n                        <audio src="/storage/music/songs/' + data.new_id + '.mp3" \n                            class="mejs__player new_song' + data.new_id + '"\n                            data-mejsoptions=\'{"pluginPath": "/path/to/shims/", "alwaysShowControls": "true"}\'>\n                        </audio>\n                        </td>\n                        \n                        <td>\n                        <button class="btn btn-secondary delete-song" name="delete-song-button"><i class="fa fa-times"></i></button>\n                        </td>\n                   </tr>\n              \n                \n                ');
    
                $("#song-progress-bar").addClass("hide");
                $("#add-song").prop("disabled", false);
                $('.new_song' + data.new_id).mediaelementplayer();
                delete_song();
                console.log(data);
            });
        });
    
        delete_song();
    });
    
    /***/ })
    
    /******/ });