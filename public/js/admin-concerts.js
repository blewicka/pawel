/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 65);
/******/ })
/************************************************************************/
/******/ ({

/***/ 65:
/***/ (function(module, exports, __webpack_require__) {

    module.exports = __webpack_require__(66);


    /***/ }),
    
    /***/ 66:
    /***/ (function(module, exports) {
    
    function delete_photo() {
        $('.delete-photo').click(function () {
    
            var photo_id = $(this).parent().parent().attr("id");
            //console.log($(this).closest('div').attr('id'));
    
            var th = this;
            $.post('/admin/gallery/delete', { id_to_remove: photo_id }).done(function (data) {
                $(th).parent().parent().remove();
            });
        });
    }
    
    function progress(e) {
        //e - akcja która się zdarza w trakcie wysyłania co kawałek
        if (e.lengthComputable) {
            //info czy da się policzyć wysyłanie
            var max = e.total; //ile jest cały
            var current = e.loaded; // ile już się wysłało
            var Percentage = current * 100 / max;
            console.log(Percentage);
            $(".progress-bar").attr('aria-valuenow', Percentage.toFixed(0));
            $(".progress-bar").attr('style', "width:" + Percentage.toFixed(0) + "%");
            $("#upload-progress").text(Percentage.toFixed(0) + " %"); //ustawia ten procent jako tekst w jakimś elemencie
            if (Percentage >= 100) {}
        }
    }
    
    var cropper;
    
    $(document).ready(function () {
    
        //obsługa zmiany napisu po dodaniu pliku do zapisania
        $('#photo-file').on('change', function () {
            $(".cropper-container").removeClass("hide");
            $("#photo-cropper").removeClass("hide");
            // console.log('zmiana pliku');
            $("#upload-photo-button").text($('#photo-file')[0].files[0].name);
    
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#photo-cropper').attr('src', e.target.result);
                var image = document.getElementById('photo-cropper');
                cropper = new Cropper(image, {
                    aspectRatio: 16 / 9,
                    crop: function crop(e) {
                        console.log(e.detail.x);
                        console.log(e.detail.y);
                        console.log(e.detail.width);
                        console.log(e.detail.height);
                        console.log(e.detail.rotate);
                        console.log(e.detail.scaleX);
                        console.log(e.detail.scaleY);
                    }
                });
            };
            reader.readAsDataURL($('#photo-file')[0].files[0]);
        });
    
        //dodawanie pliku
        $('#add-photo').click(function () {
    
            console.log('funkcja wysylania');
            var fd = new FormData(); //tworzy nową formę na dane
    
            var photo_file = $('#photo-file')[0].files[0]; //wybrany plik
            fd.append('photo', photo_file); //dodaję plik do formy 'x'-nazwa, y - wartosc:liczba, text, plik itd
    
            var photo_title = $('#photo-title').val();
            fd.append('photo_title', photo_title);
    
            if (photo_title == '' || photo_file == null) {
                alert("Wpisz tytuł i treść");
                return;
            }
    
            $("#add-photo").prop("disabled", true);
            $("#photo-progress-bar").removeClass("hide");
    
            cropper.getCroppedCanvas().toBlob(function (photo_thumb) {
                fd.append('thumb', photo_thumb);
                /*tutaj cała reszta wysyłania*/
                $.ajax({ //wysyłam plik
                    url: "save", //adres pod który chcę go wysłać
                    type: "POST", //metoda którą chcę go wysłać
                    data: fd, //dane które chcę wysłać czyli nasza forma z danymi
                    enctype: 'multipart/form-data', //typ danych
                    processData: false, // tell jQuery not to process the data
                    contentType: false, // tell jQuery not to set contentType
                    xhr: function xhr() {
                        //nie ważne, tutaj można dorobić śledzenie progresu wrzucania
                        var myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload) {
                            myXhr.upload.addEventListener('progress', progress, false);
                        }
                        return myXhr;
                    }
                }).done(function (data) {
                    //funkcja wykonywana po zakeńczeniu wysyłania
    
                    $('#photo-title').val('');
                    $('#photo-file').val('');
                    $("#upload-photo-button").html('<i class="fa fa-paperclip"></i>');
    
                    $('#admin-photos-table').find('tbody').append('\n                \n                \n                   <tr id="' + data.new_id + '">\n                        <td>\n                        ' + photo_title + ' &nbsp;&nbsp;\n                        </td>\n\n                        <td>\n                        <img class="mini-img" src="/storage/gallery/' + data.new_id + '.jpg"/>\n                        </td>\n                        \n                        <td>\n                        <button class="btn btn-secondary delete-photo" name="delete-photo-button"><i class="fa fa-times"></i></button>\n                        </td>\n                   </tr>\n              \n                \n                ');
    
                    $("#photo-progress-bar").addClass("hide");
                    $("#photo-cropper").addClass("hide");
                    $(".cropper-container").addClass("hide");
                    $("#add-photo").prop("disabled", false);
                    $('.new_photo' + data.new_id).mediaelementplayer();
                    delete_photo();
                    console.log(data);
                });
            });
        });
    
        delete_photo();
    });
    
    /***/ })
    
    /******/ });