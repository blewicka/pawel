/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 62);
/******/ })
/************************************************************************/
/******/ ({

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

    module.exports = __webpack_require__(63);
    
    
    /***/ }),
    
    /***/ 63:
    /***/ (function(module, exports) {
    
    function delete_article() {
        $('.delete-article').click(function () {
    
            var article_id = $(this).parent().parent().attr("id");
            //console.log($(this).closest('div').attr('id'));
    
            var th = this;
            $.post('/admin/media/articles/delete', { id_to_remove: article_id }).done(function (data) {
                $(th).parent().parent().remove();
            });
        });
    }
    
    function progress(e) {
        //e - akcja która się zdarza w trakcie wysyłania co kawałek
        if (e.lengthComputable) {
            //info czy da się policzyć wysyłanie
            var max = e.total; //ile jest cały
            var current = e.loaded; // ile już się wysłało
            var Percentage = current * 100 / max;
            console.log(Percentage);
            $(".progress-bar").attr('aria-valuenow', Percentage.toFixed(0));
            $(".progress-bar").attr('style', "width:" + Percentage.toFixed(0) + "%");
            $("#upload-progress").text(Percentage.toFixed(0) + " %"); //ustawia ten procent jako tekst w jakimś elemencie
            if (Percentage >= 100) {}
        }
    }
    
    $(document).ready(function () {
    
        //obsługa zmiany napisu po dodaniu pliku do zapisania
        $('#article-file').on('change', function () {
            // console.log('zmiana pliku');
            $("#upload-article-button").text($('#article-file')[0].files[0].name);
        });
    
        //dodawanie pliku
        $('#add-article').click(function () {
    
            console.log('funkcja wysylania');
            var fd = new FormData(); //tworzy nową formę na dane
            var article_file = $('#article-file')[0].files[0]; //wybrany plik
            fd.append('article', article_file); //dodaję plik do formy 'x'-nazwa, y - wartosc:liczba, text, plik itd
    
            var article_title = $('#article-title').val();
            fd.append('article_title', article_title);
    
            if (article_title == '' || article_file == null) {
                alert("Wpisz tytuł i treść");
                return;
            }
    
            $("#add-article").prop("disabled", true);
            $("#article-progress-bar").removeClass("hide");
            $.ajax({ //wysyłam plik
                url: "save", //adres pod który chcę go wysłać
                type: "POST", //metoda którą chcę go wysłać
                data: fd, //dane które chcę wysłać czyli nasza forma z danymi
                enctype: 'multipart/form-data', //typ danych
                processData: false, // tell jQuery not to process the data
                contentType: false, // tell jQuery not to set contentType
                xhr: function xhr() {
                    //nie ważne, tutaj można dorobić śledzenie progresu wrzucania
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', progress, false);
                    }
                    return myXhr;
                }
            }).done(function (data) {
                //funkcja wykonywana po zakeńczeniu wysyłania
    
                $('#article-title').val('');
                $('#article-file').val('');
                $("#upload-article-button").html('<i class="fa fa-paperclip"></i>');
    
                $('#admin-articles-table').find('tbody').append('\n                \n                \n                   <tr id="' + data.new_id + '">\n                        <td>\n                        ' + article_title + ' &nbsp;&nbsp;\n                        </td>\n\n                        <td>\n                        <img class="mini-img" src="/storage/media/articles/' + data.new_id + '.jpg"/>\n                        </td>\n                        \n                        <td>\n                        <button class="btn btn-secondary delete-article" name="delete-article-button"><i class="fa fa-times"></i></button>\n                        </td>\n                   </tr>\n              \n                \n                ');
    
                $("#article-progress-bar").addClass("hide");
                $("#add-article").prop("disabled", false);
                $('.new_article' + data.new_id).mediaelementplayer();
                delete_article();
                console.log(data);
            });
        });
    
        delete_article();
    });
    
    /***/ })
    
    /******/ });