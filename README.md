Musician portofilio (work in progress!)
========================

Welcome to the musician's portofilio website.

What's inside?
--------------

The portofilio uses (so far) :

  * PHP 7.X

  * Laravel PHP framework

  * jQuery

  * Bootstrap 4

  * Quill.js text editor
  
  * MariaDB/MySQL database
  
  * MediaElement.js video and audio player

It allows user to change content on the website from a admin panel


Feel free to follow the progress at http://aisab.duckdns.org:8000

Enjoy!