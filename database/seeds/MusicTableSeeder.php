<?php

use Illuminate\Database\Seeder;

class MusicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Music::insert([
            [
                'name' => 'Utwór 1',
                'lang' => 'pl'   
            ],
            [
                'name' => 'Track 1',
                'lang' => 'en'  
            ]
        ]);
    }
}
