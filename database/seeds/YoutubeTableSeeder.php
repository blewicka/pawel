<?php

use Illuminate\Database\Seeder;

class YoutubeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Youtube::insert([[
            'title' => 'tytul linku',
            'video_id' => 'h6sbRxzivpw',
            'lang' => 'pl'
        ],
        [
            'title' => 'tytul linku',
            'video_id' => 'URugFjhXHrs',
            'lang' => 'en'
        ]]);
    }
}
