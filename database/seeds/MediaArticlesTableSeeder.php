<?php

use Illuminate\Database\Seeder;

class MediaArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\MediaArticle::insert([
            
                        [
                            'name' => 'tytuł 1',
                            'lang' => 'pl',
                            'position' => '1'   
                        ],
                        [
                            'name' => 'title 1',
                            'position' => '1',
                            'lang' => 'en'  
                        ]
                    ]);
    }
}
