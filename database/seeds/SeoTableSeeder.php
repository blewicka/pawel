<?php

use Illuminate\Database\Seeder;

class SeoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Seo::insert([
            
            [
                'property' => 'page_title',
                'value' => 'tytuł strony',
                'lang' => 'pl'   
            ],
            [
                'property' => 'page_title',
                'value' => 'page title',
                'lang' => 'en' 
            ],
            [
                'property' => 'page_tags',
                'value' => 'tagi',
                'lang' => 'pl' 
            ],
            [
                'property' => 'page_tags',
                'value' => 'tags',
                'lang' => 'en' 
            ],
            [
                'property' => 'page_description',
                'value' => 'opis strony',
                'lang' => 'pl' 
            ],
            [
                'property' => 'page_description',
                'value' => 'page description',
                'lang' => 'en' 
            ]
        ]);
    }
}
