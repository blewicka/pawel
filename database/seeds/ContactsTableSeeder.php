<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Contact::insert([
            
            [
                'property' => 'contact_content',
                'value' => 'treść podsrtony kontakt',
                'lang' => 'pl'   
            ],
            [
                'property' => 'contact_content',
                'value' => 'contact content',
                'lang' => 'en' 
            ],
            [
                'property' => 'contact_mail',
                'value' => 'email@email.com',
                'lang' => 'pl' 
            ],
            [
                'property' => 'contact_mail',
                'value' => 'email@email.com',
                'lang' => 'en' 
            ],

        ]);
    }
}
