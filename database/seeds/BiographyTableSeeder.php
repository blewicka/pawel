<?php

use Illuminate\Database\Seeder;

class BiographyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Biography::insert([[
            'lang' => 'pl',
            'text' => 'Treść polska'
        ],
        [
            'lang' => 'en',
            'text' => 'Content english'
        ]]);
    }
}
