<?php

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Photo::insert([
            
                        [
                            'name' => 'tytuł 1',
                            'lang' => 'pl'   
                        ],
                        [
                            'name' => 'title 1',
                            'lang' => 'en'  
                        ]
                    ]);
    }
}
