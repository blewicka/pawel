<?php

use Illuminate\Database\Seeder;

class MediaMusicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\MediaMusic::insert([

            [
                'name' => 'Utwór 1',
                'lang' => 'pl'   
            ],
            [
                'name' => 'Track 1',
                'lang' => 'en'  
            ]
        ]);
    }
}
