<?php

use Illuminate\Database\Seeder;

class MediaYoutubeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\MediaYoutube::insert([[
            'title' => 'tytul linku',
            'video_id' => 'h6sbRxzivpw',
            'lang' => 'pl'
        ],
        [
            'title' => 'tytul linku',
            'video_id' => 'URugFjhXHrs',
            'lang' => 'en'
        ]]);
    }
}
