<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LinksTableSeeder::class);
        $this->call(BiographyTableSeeder::class);
        $this->call(MusicTableSeeder::class);
        $this->call(YoutubeTableSeeder::class);
        $this->call(MediaYoutubeTableSeeder::class);
        $this->call(MediaArticlesTableSeeder::class);
        $this->call(PhotosTableSeeder::class);
        $this->call(ConcertsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
    }
}
