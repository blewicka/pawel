let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app-main.js', 'public/js')
    .js('resources/assets/js/app-admin.js', 'public/js')
    .js('resources/assets/js/admin/admin-biography.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-music.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-music-videos.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-media-videos.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-media-songs.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-media-articles.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-gallery.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-concerts.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-settings.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-seo.js', 'public/js/admin')
    .js('resources/assets/js/admin/admin-contact.js', 'public/js/admin')
    .sass('resources/assets/sass/app-admin.scss', 'public/css')
    .sass('resources/assets/sass/app-main.scss', 'public/css');
    //.postCss('node_modules/tinymce/skins/lightgray/content.min.css', 'public/js/skins/lightgray');
