<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaMusic extends Model
{
    protected $table = 'mediaMusic';
    protected $primaryKey = 'id';
    protected $fillable = [
         'lang',
         'name',
         'position'
    ];
}
