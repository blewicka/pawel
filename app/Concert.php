<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concert extends Model
{
    protected $table = 'concerts';
    protected $primaryKey = 'id';
    protected $fillable = [
         'lang',
         'location',
         'description',
         'date',
         'time'
    ];
}
