<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaArticle extends Model
{
    protected $table = 'mediaArticles';
    protected $primaryKey = 'id';
    protected $fillable = [
         'lang',
         'name',
         'position'
    ];
}
