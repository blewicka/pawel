<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaYoutube extends Model
{
    protected $table = 'mediaYoutube';
    protected $primaryKey = 'id';
    protected $fillable = [
         'lang',
         'title',
         'video_id',
         'position'
    ];
}
