<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Youtube extends Model
{
    protected $table = 'youtube';
    protected $primaryKey = 'id';
    protected $fillable = [
         'lang',
         'title',
         'video_id',
         'position'
    ];
}
