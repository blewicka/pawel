<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $table = 'music';
    protected $primaryKey = 'id';
    protected $fillable = [
         'lang',
         'name',
         'position'
    ];
}
