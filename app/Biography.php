<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biography extends Model
{   protected $table = 'biography';
    protected $primaryKey = 'lang';
    protected $fillable = [
        'lang',
        'text',
    ];
}
