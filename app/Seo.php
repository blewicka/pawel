<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $table = 'seo';
    protected $primaryKey = 'id';
    protected $fillable = [
        'lang',
        'property',
        'value'
    ];
}
