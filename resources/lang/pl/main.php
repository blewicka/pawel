<?php

return [
    'biography' => 'biografia',
    'concerts' => 'koncerty',
    'music' => 'muzyka',
    'media' => 'media',
    'gallery' => 'galeria',
    'contact' => 'kontakt',
    'all-concerts' => 'Wszystkie',
    'name' =>'Imię i nazwisko',
    'email' => 'Adres e-mail',
    'phone'=>'Numer telefonu',
    'msg-content' =>'Wiadomość',
    'send'=>'Wyślij',
    'message-sent'=>'Wiadomość została wysłana. Postaram się odpowiedzieć w możliwie najkrótszym czasie.'
];

?>