<?php

return [
    'biography' => 'biography',
    'concerts' => 'concerts',
    'music' => 'music',
    'media' => 'media',
    'gallery' => 'gallery',
    'contact' => 'contact',
    'all-concerts' => 'All concerts',
    'name' =>'Name and surname',
    'email' => 'E-mail address',
    'phone'=>'Phone number',
    'msg-content' =>'Message',
    'send'=>'Send',
    'message-sent'=>'Thank you for your message. I will try to get back to you as soon as possible.'
];

?>