@extends('layouts.app')

@section ('css')
<link href="{{ asset('css/app-admin.css') }}" rel="stylesheet">
@yield('admin-css')
@endsection

@section ('js')
<script src="{{ asset('js/app-admin.js') }}"></script>
@yield('admin-js')

@endsection

@section('content')

<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
<div class="nav-side-menu">
    <div class="brand">Admin</div>
    <i class="fa fa-bars fa-2x toggle-btn"></i>
  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content">
            
                <li  data-toggle="collapse" data-target="#languages" class="collapsed">
                  <a href="#"><i class="fa fa-language"></i> Języki <span class="arrow"></span></a>
                </li>

                <ul class="sub-menu collapse show" id="languages">
                    <li><a href="../pl/">Polski</a></li>
                    <li><a href="../en/">English</a></li>
                </ul>

                <li>
                  <a href="/admin/settings/{{$curr_lang}}/">
                    <i class="fa fa-cogs"></i> Ustawienia
                  </a>
                </li>

                <li>
                  <a href="/admin/biography/{{$curr_lang}}/">
                  <i class="fa fa-id-card-o"></i> Biografia
                  </a>
                </li>

                <li>
                  <a href="/admin/concerts/{{$curr_lang}}/">
                  <i class="fa fa-calendar"></i> Koncerty
                  </a>
                </li>
                
                <li  data-toggle="collapse" data-target="#music" class="collapsed">
                  <a href="#"><i class="fa fa-music"></i> Muzyka <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="music">
                    <li><a href="/admin/music/songs/{{$curr_lang}}/">Utwory</a></li>
                    <li><a href="/admin/music/videos/{{$curr_lang}}/">Filmy</a></li>
                </ul>               

                <li  data-toggle="collapse" data-target="#media" class="collapsed">
                  <a href="#"><i class="fa fa-file-image-o"></i> Media <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="media">
                    <li><a href="/admin/media/articles/{{$curr_lang}}/">Artykuły</a></li>
                    <li><a href="/admin/media/recordings/{{$curr_lang}}/">Audio</a></li>
                    <li><a href="/admin/media/videos/{{$curr_lang}}/">Filmy</a></li>
                </ul>

                <li>
                  <a href="/admin/gallery/{{$curr_lang}}/">
                  <i class="fa fa-camera"></i> Galeria
                  </a>
                </li>

                <li>
                  <a href="/admin/seo/{{$curr_lang}}/">
                  <i class="fa fa-tags"></i> SEO
                  </a>
                </li>

                <li>
                  <a href="/admin/contact/{{$curr_lang}}/">
                  <i class="fa fa-envelope-o"></i> Kontakt
                  </a>
                </li>
                
                <li>
                  <a href="/logout">
                  <i class="fa fa-sign-out"></i> Wyloguj
                  </a>
                </li>

                
                
                
            </ul>
     </div>
</div>

<div id="admin-content">

@yield('admin-content')


</div>

@endsection



