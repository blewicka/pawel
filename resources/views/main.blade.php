@extends('layouts.app')

@section ('css')
<link href="{{ asset('css/app-main.css') }}" rel="stylesheet">
@endsection

@section ('js')
<script src="{{ asset('js/app-main.js') }}"></script>
@endsection

@section('content')
    
    
        @include('segments/welcome')

        <nav class="navbar navbar-expand-lg navbar-light bg-light" id="mainNav">
            <div class="container">
                
                <button id="navbar-button" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                    <ul class="navbar-nav">

                            <!-- <li class="nav-item"><a href="{{ route('login') }}" class="nav-link">Login</a></li> -->
                            <li class="nav-item"> <a href="#biography" class="nav-link">{{ __('main.biography') }}</a> </li>
                            <li class="nav-item"><a href="#concerts" class="nav-link">{{ __('main.concerts') }}</a></li>
                            <li class="nav-item"><a href="#music" class="nav-link">{{ __('main.music') }}</a></li>
                            <li class="nav-item"><a href="#media" class="nav-link">{{ __('main.media') }}</a></li>
                            <li class="nav-item"><a href="#gallery" class="nav-link">{{ __('main.gallery') }}</a></li>
                            <li class="nav-item"><a href="#contact" class="nav-link">{{ __('main.contact') }}</a></li>
                    
                    </ul>
                </div>

            </div>
        </nav>
    
    
        <div id="biography">
        @include('segments/biography')
        </div>

        <div class="border-image">
            <img class="img-center" src="{{ asset('storage/border-photos/biography.jpg') }}" alt="">
        </div>

        <div id="concerts">
        @include('segments/concerts')
        </div>


        <div class="border-image">
            <img class="img-center" src="{{ asset('storage/border-photos/concerts.jpg') }}" alt="">
        </div>

        <div id="music">
        @include('segments/music')
        </div>


        <div class="border-image">
            <img class="img-center" src="{{ asset('storage/border-photos/music.jpg') }}" alt="">
        </div>

        <div id="media">
        @include('segments/media')
        </div>


        <div class="border-image">
            <img class="img-center" src="{{ asset('storage/border-photos/media.jpg') }}" alt="">
        </div>

        <div id="gallery">
        @include('segments/gallery')
        </div>


        <div class="border-image">
           <img class="img-center" src="{{ asset('storage/border-photos/gallery.jpg') }}" alt="">
        </div>

        <div id="contact">
        @include('segments/contact')
        </div>

        <div id="footer"></div>
    

@endsection
