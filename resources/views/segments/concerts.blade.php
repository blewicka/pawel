<div class="segment-page">
    

    <div class="segment-title">
    {{ __('main.concerts') }}
    </div>   

    <nav class="pn-ProductNav">
        <div class="pn-ProductNav_Contents"  id="mobile-years-menu">
            
            <a class="concert-year pn-ProductNav_Link all-concerts" aria-selected="true" id="mobile-all-concerts">{{ __('main.all-concerts') }}</a>

            
            @foreach($concerts_years as $year)

            <a class="concert-year pn-ProductNav_Link @if (substr($concerts_years[0]->date, 0,4)==substr($year->date, 0,4)) active-year @endif" aria-selected="true" data-year="{{substr($year->date, 0,4)}}" id="mobile-{{substr($year->date, 0,4)}}">{{substr($year->date, 0,4)}}</a>

            @endforeach
        </div>
    </nav>


    <div class="row">
        <div class="col-sm-0 col-md-4 col-lg-4" id="desktop-years-menu" align="center">
            <ul class="list-group" id="years-menu">
            
            <li class="list-group-item concert-year all-concerts"  id="desktop-all-concerts"><a>{{ __('main.all-concerts') }}</a></li>
            @foreach($concerts_years as $year)

            <li class="list-group-item concert-year @if (substr($concerts_years[0]->date, 0,4)==substr($year->date, 0,4)) active-year @endif" data-year="{{substr($year->date, 0,4)}}" id="desktop-{{substr($year->date, 0,4)}}"><a>{{substr($year->date, 0,4)}}</a></li>
            
            @endforeach
            </ul> </br>
        </div>
        
        <div class="col-sm-12 col-md-12 col-lg-8" align="center">
            @foreach($concerts_list as $concert)
            <div class="row {{substr($concert->date, 0,4)}} @if (substr($concerts_years[0]->date, 0,4)!=substr($concert->date, 0,4)) hide @endif concert">
                <div id=" {{ substr($concerts_years[0]->date, 0,4) }} "> </div>
                <div class="col-sm-0 col-md-0 col-lg-8 concert-info" >
                    <b> {{ \Carbon\Carbon::parse($concert->date)->format('d-m-Y')}} {{ substr($concert->time,0,5) }} </b> </br>
                    <div class="concert_description">
                    <b> {!!$concert->location!!} </b></br></br>
                    
                        {!!$concert->description!!}
                    </div>
                </div>
            
                <div class="col-4 concert-poster" align="center">
                        <img class="mini-img" src="{{ asset('storage/concerts/'.$concert->id.'.jpg') }}"/>
                </div>
                <hr>
            </div>
            @endforeach
            <img id="up-arrow" src="{{ asset('images/przyciskup.png') }}"/>
        </div>
    </div>
    
</div>



