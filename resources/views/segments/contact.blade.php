

        <div class="segment-page">
            
        
        <div class="segment-title">
        {{ __('main.contact') }}
        </div>     
            <div id="contact-info">
                {!!$contact_content->value!!}
            </div>
            

            <form onSubmit="return false;" id="contact-form">
                    <div class="form-group">
                        <input type="text" class="form-control" id="contact-name" name="name" placeholder="{{ __('main.name') }}" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="contact-email" name="email" placeholder="{{ __('main.email') }}" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="contact-phone" name="phone" placeholder="{{ __('main.phone') }}" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="msg-content" rows="5" name="content" placeholder="{{ __('main.msg-content') }}" required></textarea>
                    </div>
                    
                    <button id="send-msg-button" class="btn btn-info">{{ __('main.send') }}</button>
            </form>

            <div id="message-sent" class="hide">
                 {{ __('main.message-sent') }}
            </div>

        </div>



