        <div class="segment-page">
            
        
        <div class="segment-title">
        {{ __('main.music') }}
        </div>     


       
            <table class="table recordings-table">   
                <tbody>
                    @foreach($music_list as $song)  
                        <tr>   
                            <td align="left">
                                <i class="fa fa-music"></i> {!! $song->name !!} 
                            </td>
                        

                            <td> 
                                <audio src="{{ asset('storage/music/songs/'.$song->id.'.mp3') }}" 
                                    class="mejs__player"
                                    data-mejsoptions='{"pluginPath": "/path/to/shims/", "alwaysShowControls": "true"}'>               
                                </audio> 
                            </td>

                        </tr>
                    @endforeach         
                </tbody>        
            </table>


          
        <div class="music-display">          
            <div class="row">
                @foreach($video_list as $video)
                <div class="col-lg-4 col-md-6 col-xs-12 video">
                   
                        <iframe src="https://www.youtube.com/embed/{{ $video->video_id }}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    
                </div>          
                @endforeach  
            </div>
        </div>        



        </div>



