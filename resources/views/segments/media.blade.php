

<div class="segment-page">
            
        
        <div class="segment-title">
        {{ __('main.media') }}
        </div>     

        <div id="articles">
            <div id="article" class="overlay hide">
                <a href="javascript:void(0)" class="closebtn closebtn-article">&times;</a>
                <div class="overlay-content">
                    <div class="ovelay-arrow left-arrow article-prev"><i class="fa fa-angle-left"></i></div>
                    <div class="ovelay-arrow right-arrow article-next"><i class="fa fa-angle-right"></i></div>
                    <img class="article-image" src="#"/>
                </div>
            </div>
            @foreach($media_articles_list as $article)
                

            
            <span class="openArticle article-link" id="{{$article->id}}" data-id="{{$article->id}}"> <i class="fa fa-file-text-o article-icon"></i>  {!! $article->name !!}
                <img class="gallery-blur hide" src="{{ asset('storage/media/articles/'.$article->id.'-blur.jpg') }}"/>
            </span>
            </br>
               
            @endforeach  

        </div>

    <table class="table recordings-table">   
        <tbody>
                @foreach($media_recordings_list as $recording)
    
                    <tr id="{{ $recording->id }}">   
                        <td align="left">
                            {!! $recording->name !!} 
                        </td>
                    
    
                        <td> 
                            <audio src="{{ asset('storage/media/songs/'.$recording->id.'.mp3') }}" 
                                class="mejs__player"
                                data-mejsoptions='{"pluginPath": "/path/to/shims/", "alwaysShowControls": "true"}'>               
                            </audio> 
                        </td>
    
                    
                @endforeach        
        </tbody>        
    </table>


        <div class="music-display">          
            <div class="row">
                @foreach($media_video_list as $media_video)
                <div class="col-lg-4 col-md-6 col-xs-12 video">
                   
                        <iframe src="https://www.youtube.com/embed/{{ $media_video->video_id }}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    
                </div>          
                @endforeach  
            </div>
        </div>        

</div>



