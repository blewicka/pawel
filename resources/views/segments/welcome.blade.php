

        <div class="welcome-page">
        <div class="lang-buttons nav-item nav-link">
        <a href="/pl">
                    <img class="lang-button @if ($lang=='pl') active-lang @endif" alt="PL" src="{{ asset('images/icons/pl-flag.png') }}">
            </a>
            
            <a href="/en">
                <img class="lang-button @if ($lang=='en') active-lang @endif" alt="EN" src="{{ asset('images/icons/en-flag.png') }}">
            </a>
        </div>
            <div id="welcome-content">
                <div id="main-logo">
                    <img id="img-logo" class="img-fluid" src="{{ asset('images/logo-'.$lang.'.png') }}" alt="logo"> 
                </div>  
                <div class="main-image">
                    <img id="img-welcome" src="{{ asset('storage/border-photos/main.jpg') }}" alt="Smiley face">
                </div>


            </div>
        </div>


