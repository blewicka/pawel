

<div class="segment-page">
        

        <div class="segment-title">
        {{ __('main.gallery') }}
        </div>   

        <div class="music-display">          
                <div class="row">
                
                <div id="all-overlay">
                        <div id="full-photo" class="overlay hide">
                                <a href="javascript:void(0)" class="closebtn-gallery closebtn">&times;</a>
                                <div class="overlay-content">
                                        <div class="ovelay-arrow left-arrow gallery-prev"><i class="fa fa-angle-left"></i></div>
                                        <img class="gallery-photo" src=""/>
                                        <div class="ovelay-arrow right-arrow gallery-next"><i class="fa fa-angle-right"></i></div>
                                </div>
                        </div>
                        {{--
                        @foreach($photos_list as $photo)
                               
                                <div id="full-photo{{ $photo->id }}" class="overlay hide">
                                        <img class="gallery-blur hide" src="{{ asset('storage/gallery/'.$photo->id.'-blur.jpg') }}"/>
                                        <a href="javascript:void(0)" class="closebtn">&times;</a>
                                        <div class="overlay-content">
                                                <div class="ovelay-arrow left-arrow gallery-prev"><i class="fa fa-angle-left"></i></div>
                                                <img class="gallery-photo" src="{{ asset('storage/gallery/'.$photo->id.'.jpg') }}"/>
                                                <div class="ovelay-arrow right-arrow gallery-next"><i class="fa fa-angle-right"></i></div>
                                        </div>
                                </div>
                        @endforeach 
                        --}}
                </div>

                @foreach($photos_list as $photo)
                        <div class="col-lg-4 col-md-6 col-xs-12 video">
                                <span style="font-size:18px;cursor:pointer" class="openPhoto" id="photo{{$photo->id}}" data-id="{{$photo->id}}"> 
                                        <img class="gallery-thumb" src="{{ asset('storage/gallery/'.$photo->id.'-thumb.jpg') }}"/>
                                </span>    
                        </div>          
                @endforeach  
                </div>
        </div> 
          
        
</div>

