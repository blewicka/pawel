@extends('layouts.admin')


@section('admin-js')
    <script src="{{ asset('js/admin/admin-media-articles.js') }}"></script>
@endsection

@section('admin-content')

<div class="admin-center">
    <div class="add-music-form">
        <label for="exampleInputFile"><b>MEDIA/DODAJ ARTYKUŁ/wersja: {{$curr_lang}}</b></label>
        
        
        <div class="input-group article-input">
            <span class="input-group-btn article-input-button">

                <label for="article-file" class="btn btn-secondary" id="upload-article-button">
                    <i class="fa fa-paperclip"></i>
                </label>               
                <input type="file"  id="article-file"/>

            </span>
        </div>
            <div class="article-editor article-input">
                <textarea rows="5" id="article-title"> </textarea>
            </div>
        
        <div class="input-group article-input">
            <span class="input-group-btn article-input-button">
                <button type="submit" class="btn btn-secondary" id="add-article" name="add-article-button">Dodaj</button>
            </span>
        </div>
            
        </br>

        
            <div class="progress hide" id="article-progress-bar">
                <div class="progress-bar progress-bar-striped progress-bar-animated" id="upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                </div>
            </div>
        
        
        
    </div>

    <div class="delete-music-form">  
    <table class="table table-bordered table-striped" id="admin-articles-table">   
    <thead>
        <tr class="table-primary">
            <th> Nazwa </th>
            <th> Zdjęcie </th>
            <th> Usuń </th>
        </tr>
    </thead>
        <tbody>
            @foreach($media_articles_list as $article)

                <tr id="{{ $article->id }}">   
                    <td>
                        {!! $article->name !!} 
                    </td>
                

                    <td> 
                        <img class="mini-img" src="{{ asset('storage/media/articles/'.$article->id.'.jpg') }}"/>

                    </td>

                    <td>
                        <button class="btn btn-secondary delete-article" name="delete-article-button"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                
            @endforeach  
        </tbody>   
    </table>
</div>

</div> 
@endsection