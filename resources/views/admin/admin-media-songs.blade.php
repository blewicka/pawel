@extends('layouts.admin')


@section('admin-js')
    <script src="{{ asset('js/admin/admin-media-songs.js') }}"></script>
@endsection

@section('admin-content')

<div class="admin-center">
<div class="add-music-form">
    <label for="exampleInputFile"><b>MEDIA/DODAJ MP3/wersja: {{$curr_lang}}</b></label>
    
    
    <div class="input-group song-input-width">
        <span class="input-group-btn song-input-width">

            <label for="song-file" class="btn btn-secondary song-input-width" id="upload-song-button">
                <i class="fa fa-paperclip"></i>
            </label>               
            <input type="file"  id="song-file"/>

        </span>
    </div>   
        
     Tytuł nagrania:</br>
            <div class="input-group song-input-width">
                <textarea rows="1" id="song-title"> </textarea>
            </div>
    <div class="input-group song--width">
        <span class="input-group-btn song-input-width">
            <button type="submit" class="btn btn-secondary song-input-width" id="add-song" name="add-song-button">Dodaj</button>
        </span>
        
    </div></br>

    <div class="progress hide" id="song-progress-bar">
        <div class="progress-bar progress-bar-striped progress-bar-animated" id="upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
        </div>
    </div>
    
    
</div>

<div class="delete-music-form">  
    <table class="table table-bordered table-striped" id="admin-songs-table">   
    <thead>
        <tr class="table-primary">
            <th> Nazwa </th>
            <th> Film </th>
            <th> Usuń </th>
        </tr>
    </thead>
        <tbody>
            @foreach($media_recordings_list as $recording)

                <tr id="{{ $recording->id }}">   
                    <td>
                        {!!$recording->name !!} 
                    </td>
                

                    <td> 
                        <audio src="{{ asset('storage/media/songs/'.$recording->id.'.mp3') }}" 
                            class="mejs__player"
                            data-mejsoptions='{"pluginPath": "/path/to/shims/", "alwaysShowControls": "true"}'>               
                        </audio> 
                    </td>

                    <td>
                        <button class="btn btn-secondary delete-song" name="delete-song-button"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                
            @endforeach  
        </tbody>   
    </table>
</div>
</div> 
@endsection