@extends('layouts.admin')


@section('admin-js')
<script src="{{ asset('js/admin/admin-contact.js') }}"></script>
@endsection

@section('admin-content')
<div class="admin-center">
    <div class="editor">
        <label><b>KONTAKT/wersja: {{$curr_lang}}</b></label></br></br>
        <textarea rows="20"> {{$contact_content->value}} </textarea></br>
        

        E-mail:</br>
        <div class="input-group">
            
                    
                <input type="text" class="form-control" id="contact-mail" placeholder="{{$contact_mail->value}}"/>
            
                <button type="submit" class="btn btn-secondary form-control" id="save-mail">Zapisz adres e-mail</button>
        
        </div>
        <div class="alerts">
        </div>

    </div>
</div>
@endsection