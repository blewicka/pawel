@extends('layouts.admin')


@section('admin-js')
    <script src="{{ asset('js/admin/admin-media-videos.js') }}"></script>
@endsection

@section('admin-content')

<div class="admin-center">
    <div class="add-video-form">
        <label for="exampleInputFile"><b>Media/DODAJ LINK YOUTUBE/wersja: {{$curr_lang}}</b></label>
        
        
        <div class="input-group">

            <input type="text" class="form-control" id="video-title" placeholder="tytuł filmu"/>
            <input type="text" class="form-control" id="video-link-id" placeholder="youtube ID filmu"/>
            <span class="input-group-btn">
                <button type="submit" class="btn btn-secondary" id="add-video" name="add-video-button">Dodaj</button>
            </span>
            
        </div></br>
      
        
    </div>
    


    <div class="delete-video-form">  
        <table class="table table-striped table-bordered" id="admin-video-table">   
            
            <thead>
                <tr class="table-primary">
                    <th> Nazwa </th>
                    <th> Film </th>
                    <th> Usuń </th>
                </tr>
            </thead>

            <tbody>
            @foreach($media_video_list as $media_video)
       
                <tr id="{{ $media_video->id }}">   
                    <td>
                        {{ $media_video->title }} 
                    </td>
                

                    <td>

                    <iframe width="280" height="120" src="https://www.youtube.com/embed/{{ $media_video->video_id }}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        
                    </td>

                    <td>
                        <button class="btn btn-secondary delete-video" name="delete-video-button"><i class="fa fa-times"></i></button>
                    </td>
                </tr>

            @endforeach  
        </tbody>    
        </table>
    </div>
       
</div> 
@endsection

