@extends('layouts.admin')


@section('admin-js')
<script src="{{ asset('js/admin/admin-biography.js') }}"></script>
@endsection

@section('admin-content')

<div class="editor">
<label><b>BIOGRAFIA/wersja: {{$curr_lang}}</b></label></br></br>
    <textarea rows="30">{{ $biography_text }}</textarea></br>
</div>

@endsection