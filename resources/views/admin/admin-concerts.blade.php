@extends('layouts.admin')

@section('admin-js')
    <script src="{{ asset('js/admin/admin-concerts.js') }}"></script>
@endsection


@section('admin-content')

<div class="admin-center">
    <div class="add-photo-form">
        <label><b>KONCERTY/DODAJ KONCERT/wersja: {{$curr_lang}}</b></label></br></br>
        
        
        <div class="input-group">

            <span class="input-group-btn" id="input-concert">
                <label for="poster-file" class="btn btn-secondary" id="upload-poster-button">
                    <i class="fa fa-paperclip"> Dodaj plakat </i>
                </label> 
                <input type="file"  id="poster-file"/>
            </span> 

            
                <input type="date" class="form-control date-pick" name="bday" id="concert-date">
                <input type="time" class="form-control date-pick" name="usr_time" id="concert-time">
        </div></br>

    
        Loklizacja:</br>
        <div class="concert-editor">
            <textarea rows="1" id="concert-location"  placeholder="Lokalizacja"> </textarea>
        </div></br>

        Opis:</br>
        <div class="concert-editor">
            <textarea id="concert-descripiton" rows="5" placeholder="Program koncertu"> </textarea>
        </div>

                
        <button type="submit" class="btn btn-secondary form-control" id="add-concert" name="add-concert-button">Dodaj</button>
        <button type="submit" class="btn btn-secondary form-control" id="edit-concert" name="add-concert-button" style="display: none">Zapisz</button>
        </br></br>
        
                

            <div class="progress hide" id="concert-progress-bar">
                <div class="progress-bar progress-bar-striped progress-bar-animated" id="upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                </div>
            </div>

        
    </div>

    <div class="delete-concert-form">  
    <table class="table table-bordered table-striped" id="admin-concerts-table">   
    <thead>
        <tr class="table-primary">
            <th> Termin </th>
            <th> Miejsce </th>
            <th> Program </th>
            <th> Plakat </th>
            <th> Usuń </th>
        </tr>
    </thead>
        <tbody>
            @foreach($concerts_list as $concert)

                <tr id="{{ $concert->id }}">   
                    <td class="date">
                        {{ \Carbon\Carbon::parse($concert->date)->format('d-m-Y')}} {{ substr($concert->time,0,5) }} 
                    </td>

                    <td class="location">
                        {!! $concert->location !!} 
                    </td>

                    <td class="description">
                        {!! $concert->description !!} 
                    </td>

                    <td class="image"> 
                        <img class="mini-img" src="{{ asset('storage/concerts/'.$concert->id.'.jpg') }}"/>

                    </td>

                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button class="btn btn-secondary edit-concert" name="delete-concert-button"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-secondary delete-concert" name="delete-concert-button"><i class="fa fa-times"></i></button>
                        </div>
                    </td>
                </tr>

            @endforeach  
            
        </tbody>   
    </table>
    
</div>

</div> 

@endsection