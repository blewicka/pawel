@extends('layouts.admin')

@section('admin-js')
    <script src="{{ asset('js/admin/admin-settings.js') }}"></script>
@endsection

@section('admin-content')

<div class="admin-center">

    Główne zdjęcie</br>
    <div class="input-group">

        <span class="input-group-btn">
            <label for="main-border-photo" class="btn btn-secondary upload-button" id="upload-main-border-photo-button" class="upload-button">
                <i class="fa fa-paperclip"></i>
            </label>               
            <input type="file"  id="main-border-photo" class="upload-border-file"/>
        </span>
        
        <span class="input-group-btn">
            <button type="submit" class="btn btn-secondary add-border-photo" id="main" name="add-main-border-photo-button" >Dodaj</button>
        </span> 
        
    </div>
    
    <div class="progress hide" id="main-progress-bar">
        <div class="progress-bar progress-bar-striped progress-bar-animated upload-progress" id="main-upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
        </div>
    </div>
    </br>
    <div id="main-border-image">
        <img class="border-mini" src="{{ asset('storage/border-photos/main.jpg') }}"/><hr>
    </div>

    </br>

    Biografia/Koncerty</br>
    <div class="input-group">

        <span class="input-group-btn">
            <label for="biography-border-photo" class="btn btn-secondary upload-button" id="upload-biography-border-photo-button" class="upload-button">
                <i class="fa fa-paperclip"></i>
            </label>               
            <input type="file"  id="biography-border-photo" class="upload-border-file"/>
        </span>
        
        <span class="input-group-btn">
            <button type="submit" class="btn btn-secondary add-border-photo" id="biography" name="add-biography-border-photo-button" >Dodaj</button>
        </span> 
        
    </div>
    
    <div class="progress hide" id="biography-progress-bar">
        <div class="progress-bar progress-bar-striped progress-bar-animated upload-progress" id="biography-upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
        </div>
    </div>
    </br>
    <div id="biography-border-image">
        <img class="border-mini" src="{{ asset('storage/border-photos/biography.jpg') }}"/><hr>
    </div>

    </br>
    

    Koncerty/Muzyka</br>
    <div class="input-group">

        <span class="input-group-btn">
            <label for="concerts-border-photo" class="btn btn-secondary upload-button" id="upload-concerts-border-photo-button" class="upload-button">
                <i class="fa fa-paperclip"></i>
            </label>               
            <input type="file"  id="concerts-border-photo" class="upload-border-file"/>
        </span>
        
        <span class="input-group-btn">
            <button type="submit" class="btn btn-secondary add-border-photo" id="concerts" name="add-concerts-border-photo-button" >Dodaj</button>
        </span>

    </div>

    <div class="progress hide" id="concerts-progress-bar">
        <div class="progress-bar progress-bar-striped progress-bar-animated upload-progress" id="concerts-upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
        </div>
    </div>
    </br>
    <div id="concerts-border-image">
        <img class="border-mini" src="{{ asset('storage/border-photos/concerts.jpg') }}"/><hr>
    </div>

    </br>

    Muzyka/Media</br>
    <div class="input-group">

        <span class="input-group-btn">
            <label for="music-border-photo" class="btn btn-secondary upload-button" id="upload-music-border-photo-button" class="upload-button">
                <i class="fa fa-paperclip"></i>
            </label>               
            <input type="file"  id="music-border-photo" class="upload-border-file"/>
        </span>
        
        <span class="input-group-btn">
            <button type="submit" class="btn btn-secondary add-border-photo" id="music" name="add-music-border-photo-button" >Dodaj</button>
        </span>

    </div>

    <div class="progress hide" id="music-progress-bar">
        <div class="progress-bar progress-bar-striped progress-bar-animated upload-progress" id="music-upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
        </div>
    </div>
    </br>
    <div id="music-border-image">
        <img class="border-mini" src="{{ asset('storage/border-photos/music.jpg') }}"/><hr>
    </div>

    </br>

    Media/Galeria</br>
    <div class="input-group">

        <span class="input-group-btn">
            <label for="media-border-photo" class="btn btn-secondary upload-button" id="upload-media-border-photo-button" class="upload-button">
                <i class="fa fa-paperclip"></i>
            </label>               
            <input type="file"  id="media-border-photo" class="upload-border-file"/>
        </span>
        
        <span class="input-group-btn">
            <button type="submit" class="btn btn-secondary add-border-photo" id="media" name="add-media-border-photo-button" >Dodaj</button>
        </span>

    </div>
    
    <div class="progress hide" id="media-progress-bar">
        <div class="progress-bar progress-bar-striped progress-bar-animated upload-progress" id="media-upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
        </div>
    </div>
    </br>
    <div id="media-border-image">
        <img class="border-mini" src="{{ asset('storage/border-photos/media.jpg') }}"/><hr>
    </div>

    </br>

    Galeria/Płyta (Kontakt)</br>
    <div class="input-group">

        <span class="input-group-btn">
            <label for="gallery-border-photo" class="btn btn-secondary upload-button" id="upload-gallery-border-photo-button" class="upload-button">
                <i class="fa fa-paperclip"></i>
            </label>               
            <input type="file"  id="gallery-border-photo" class="upload-border-file"/>
        </span>
        
        <span class="input-group-btn">
            <button type="submit" class="btn btn-secondary add-border-photo" id="gallery" name="add-gallery-border-photo-button" >Dodaj</button>
        </span>

    </div>

    <div class="progress hide" id="gallery-progress-bar">
        <div class="progress-bar progress-bar-striped progress-bar-animated upload-progress" id="gallery-upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
        </div>
    </div>
    </br>
    <div id="gallery-border-image">
        <img class="border-mini" src="{{ asset('storage/border-photos/gallery.jpg') }}"/><hr>
    </div>

    </br>

    Płyta/Kontakt</br>
    <div class="input-group">

        <span class="input-group-btn">
            <label for="cd-border-photo" class="btn btn-secondary upload-button" id="upload-cd-border-photo-button" class="upload-button">
                <i class="fa fa-paperclip"></i>
            </label>               
            <input type="file"  id="cd-border-photo" class="upload-border-file"/>
        </span>
        
        <span class="input-group-btn">
            <button type="submit" class="btn btn-secondary add-border-photo" id="cd" name="add-cd-border-photo-button" >Dodaj</button>
        </span>

    </div>
    
    <div class="progress hide" id="cd-progress-bar">
        <div class="progress-bar progress-bar-striped progress-bar-animated upload-progress" id="cd-upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
        </div>
    </div>
    </br>
    <div id="cd-border-image">
        <img class="border-mini" src="{{ asset('storage/border-photos/cd.jpg') }}"/><hr>
    </div>

    </br>

</div>
@endsection