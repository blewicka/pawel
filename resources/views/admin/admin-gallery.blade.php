@extends('layouts.admin')

@section('admin-js')
    <script src="{{ asset('js/admin/admin-gallery.js') }}"></script>
@endsection


@section('admin-content')

<div class="admin-center">
    <div class="add-photo-form">
        <label for="exampleInputFile"><b>GALERIA/DODAJ ZDJĘCIE/wersja: {{$curr_lang}}</b></label>
        
        
        <div class="input-group">


            <span class="input-group-btn">

                <label for="photo-file" class="btn btn-secondary" id="upload-photo-button">
                    <i class="fa fa-paperclip"></i>
                </label>               
                <input type="file"  id="photo-file"/>

            </span>
            <input type="text" class="form-control" id="photo-title" placeholder="nazwa zdjęcia"/>
            <span class="input-group-btn">
                <button type="submit" class="btn btn-secondary" id="add-photo" name="add-photo-button">Dodaj</button>
            </span>
            
        </div></br>

        <div class="progress hide" id="photo-progress-bar">
            <div class="progress-bar progress-bar-striped progress-bar-animated" id="upload-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
            </div>
        </div>

        <img id="photo-cropper" class="hide" src="#" />
        
        
    </div>

    <div class="delete-photo-form">  
    <table class="table table-bordered table-striped" id="admin-photos-table">   
    <thead>
        <tr class="table-primary">
            <th> Nazwa </th>
            <th> Zdjęcie </th>
            <th> Usuń </th>
        </tr>
    </thead>
        <tbody>
        @foreach($photos_list as $photo)

                <tr id="{{ $photo->id }}">   
                    <td>
                        {{ $photo->name }} 
                    </td>
                

                    <td> 
                        <img class="mini-img" src="{{ asset('storage/gallery/'.$photo->id.'.jpg') }}"/>

                    </td>

                    <td>
                        <button class="btn btn-secondary delete-photo" name="delete-photo-button"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                
            @endforeach  
        </tbody>   
    </table>
</div>

</div> 

@endsection