@extends('layouts.admin')

@section('admin-js')
    <script src="{{ asset('js/admin/admin-seo.js') }}"></script>
@endsection

@section('admin-content')

<div class="admin-center">

    <div class="form-group">
        <label for="page-title">Tytuł strony:</label>
        <textarea type="text" class="form-control" id="title" rows="1">{{$page_title->value}}</textarea>
    </div>
    <div class="form-group">
        <label for="tags">Tagi:</label>
        <textarea type="text" class="form-control" id="tags" rows="3">{{$page_tags->value}}</textarea>
    </div>
    <div class="form-group">
        <label for="description">Opis:</label>
        <textarea type="text" class="form-control" id="description" row="5">{{$page_description->value}}</textarea>
    </div>
    <button type="submit" class="btn btn-secondary form-control" id="save-seo">Zapisz</button>
 

    

</div>
@endsection