function delete_song() {
    $('.delete-song').click(function() {
        
        var song_id=($(this).parent().parent().attr("id"));
        //console.log($(this).closest('div').attr('id'));

        var th = this;
        $.post('/admin/music/songs/delete/', { id_to_remove: song_id }).done(function( data ){ 
            $(th).parent().parent().remove();
        });         
    });
}

function progress(e) { //e - akcja która się zdarza w trakcie wysyłania co kawałek
    if (e.lengthComputable) { //info czy da się policzyć wysyłanie
        var max = e.total; //ile jest cały
        var current = e.loaded; // ile już się wysłało
        var Percentage = current * 100 / max;
        console.log(Percentage);
        $(".progress-bar").attr('aria-valuenow',  Percentage.toFixed(0));
        $(".progress-bar").attr('style', "width:"+Percentage.toFixed(0)+"%");
        $("#upload-progress").text(Percentage.toFixed(0) + " %"); //ustawia ten procent jako tekst w jakimś elemencie
        if (Percentage >= 100) {}
    }
}

$( document ).ready(function() { 
    

    tinymce.init({
        selector: "textarea",
        toolbar: 'fontsizeselect',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        content_style: "p{margin-top: 0!important; margin-bottom: 0rem!important;}"
    });

    //sortowanie
    $("#admin-songs-table").find('tbody').sortable({
        items: "> tr",
        appendTo: "parent",
        start: function() {
           
         },
        stop: function( event ) {
            var sort = [];
            $('#admin-songs-table').find('tbody').find('tr').each(function(index, val) {
                var sorted_id = $(this).attr('id');
                sort.push(sorted_id);
            })
            $.post('sort-recordings/', { sorted_ids: sort}, function() {console.log('recordings sorted')});
        },
        update: function() {
           
         },
    });

    //obsługa zmiany napisu po dodaniu pliku do zapisania
    $('#song-file').on('change', function() {
       // console.log('zmiana pliku');
        $( "#upload-song-button" ).text($('#song-file')[0].files[0].name);
    });

    
    //dodawanie pliku
    $('#add-song').click(function() {

               
        //console.log('funkcja wysylania');
        var fd = new FormData(); //tworzy nową formę na dane
        var song_file = $('#song-file')[0].files[0]; //wybrany plik
        fd.append('song', song_file); //dodaję plik do formy 'x'-nazwa, y - wartosc:liczba, text, plik itd

        var song_title =  tinymce.activeEditor.getContent();
        fd.append('song_title', song_title); 
        console.log(song_file);
        //console.log(song_title);

        if (song_title=='' || song_file==null){
            alert("Wpisz tytuł i treść");
            return;
        }


        $( "#add-song" ).prop( "disabled",true );
        $( "#song-progress-bar" ).removeClass( "hide" );
        $.ajax({ //wysyłam plik
                    url: "save/", //adres pod który chcę go wysłać
                    type: "POST", //metoda którą chcę go wysłać
                    data: fd, //dane które chcę wysłać czyli nasza forma z danymi
                    enctype: 'multipart/form-data', //typ danych
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                    xhr: function() { //nie ważne, tutaj można dorobić śledzenie progresu wrzucania
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                }).done(function( data ){ 
                //funkcja wykonywana po zakeńczeniu wysyłania
                
                $('#song-title').val('');
                $('#song-file').val('');
                $( "#upload-song-button" ).html('<i class="fa fa-paperclip"></i>');

                $('#admin-songs-table').find('tbody').append(`
                
                
                   <tr id="${data.new_id}">
                        <td>
                        ${song_title} &nbsp;&nbsp;
                        </td>

                        <td>
                        <audio src="/storage/music/songs/${data.new_id}.mp3" 
                            class="mejs__player new_song${data.new_id}"
                            data-mejsoptions='{"pluginPath": "/path/to/shims/", "alwaysShowControls": "true"}'>
                        </audio>
                        </td>
                        
                        <td>
                        <button class="btn btn-secondary delete-song" name="delete-song-button"><i class="fa fa-times"></i></button>
                        </td>
                   </tr>
              
                
                `);
                
                $( "#song-progress-bar" ).addClass( "hide" );
                $( "#add-song" ).prop( "disabled",false);
                $('.new_song'+data.new_id).mediaelementplayer();
                delete_song();
                console.log(data);


                });
    });

    delete_song();
  
});
    

    