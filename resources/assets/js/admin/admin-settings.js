
function progress(e) { //e - akcja która się zdarza w trakcie wysyłania co kawałek
    if (e.lengthComputable) { //info czy da się policzyć wysyłanie
        var max = e.total; //ile jest cały
        var current = e.loaded; // ile już się wysłało
        var Percentage = current * 100 / max;
        console.log(Percentage);
        $(".progress-bar").attr('aria-valuenow',  Percentage.toFixed(0));
        $(".progress-bar").attr('style', "width:"+Percentage.toFixed(0)+"%");
        $(".upload-progress").text(Percentage.toFixed(0) + " %"); //ustawia ten procent jako tekst w jakimś elemencie
        if (Percentage >= 100) {}
    }
}




$( document ).ready(function() { 

    
    tinymce.init({
        selector: "textarea",
        toolbar: 'fontsizeselect',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt'
    });

    //obsługa zmiany napisu po dodaniu pliku do zapisania
    $('.upload-border-file').on('change', function() {
        var curr_upload = $(this).attr('id');
        console.log(curr_upload);
        $( '#'+'upload-'+curr_upload+'-button').text($('#'+curr_upload)[0].files[0].name);
    });



    //dodawanie pliku
    $('.add-border-photo').click(function() {

        var curr_photo = $(this).attr('id');

        console.log(curr_photo);
        var fd = new FormData();

        var photo_to_save = $('#'+curr_photo+'-border-photo')[0].files[0]; 
        fd.append('photo', photo_to_save); 
        fd.append('title', curr_photo);

        if ( photo_to_save==null){
            alert("Wybierz zdjęcie");
            return;
        }

        $( "#"+curr_photo ).prop( "disabled",true );
        $( "#"+curr_photo+"-progress-bar" ).removeClass( "hide" );

        $.ajax({
                    url: "/admin/settings/save/", 
                    type: "POST", 
                    data: fd, 
                    enctype: 'multipart/form-data', 
                    processData: false,  
                    contentType: false,   
                    xhr: function() { 
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                }).done(function( data ){ 
                
                
                
                $(".upload-border-file").val('');
                $( ".upload-button" ).html('<i class="fa fa-paperclip"></i>');
                      
                $('#'+curr_photo+'-border-image').find('img').attr('src', "/storage/border-photos/"+curr_photo+".jpg?t=" + new Date().getTime());
                          
                $( "#"+curr_photo+"-progress-bar" ).addClass( "hide" );
                $( '#'+curr_photo ).prop("disabled", false );
                
                
                console.log(data);


                });
    });

    

    
  
});
    

    