function delete_video() {
    $('.delete-video').click(function() {
        
        var video_id=($(this).parent().parent().attr("id"));
        //console.log($(this).closest('div').attr('id'));

        var th = this;
        $.post('/admin/media/videos/delete/', { id_to_remove: video_id }).done(function( data ){ 
            $(th).parent().parent().remove();
        });         
    });
}

$( document ).ready(function() { 



    //sortowanie
    $("#admin-video-table").find('tbody').sortable({
        items: "> tr",
        appendTo: "parent",
        start: function() {
           
         },
        stop: function( event ) {
            var sort = [];
            $('#admin-video-table').find('tbody').find('tr').each(function(index, val) {
                var sorted_id = $(this).attr('id');
                sort.push(sorted_id);
            })
            $.post('sort-videos/', { sorted_ids: sort}, function() {console.log('recordings sorted')});
        },
        update: function() {
           
         },
    });

    
    $('#add-video').click(function() {
        
        
        var v_title = $('#video-title').val();
        var v_link_id = $('#video-link-id').val();
        

        if (v_title=='' || v_link_id==''){
            alert("Wpisz tytuł i id linku");
            return;
        }


        $( "#add-video" ).prop( "disabled",true );
        
        $.post('save/', { video_title: v_title, video_link_id: v_link_id }, function(data) {
            
            $('#video-title').val('');
            $('#video-link-id').val('');
            
            console.log(data.new_id);
            $('#admin-video-table').find('tbody').append(`                   
               <tr id="${data.new_id}">   
                    <td>
                       ${v_title} &nbsp;&nbsp;
                    </td>
                

                    <td>

                    <iframe width="280" height="120" src="https://www.youtube.com/embed/${v_link_id}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        
                    </td>

                    <td>
                        <button class="btn btn-secondary delete-video" name="delete-video-button"><i class="fa fa-times"></i></button>
                    </td>
               </tr>          
            `);

            
            $( "#add-video" ).prop( "disabled",false);
            
            delete_video();
        });
      
    });

    delete_video();
  
});
    

    