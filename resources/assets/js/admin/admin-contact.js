function succesalet(message) {
    var id =  Math.random().toString(36).substr(2, 9);
    $('.alerts').append("<div id=\""+id+"\" class=\"alert alert-success animated sr-only\" role=\"alert\">\
         <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\
    <span id=\"alerticon\" class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>\
    <span id=\"alertmessage\">"+message+"</span>");
    $('#'+id).addClass('slideInDown');
    $('#'+id).removeClass('sr-only');
    createAutoClosingAlert(id, 5000);
    }
    
function createAutoClosingAlert(selector, delay) {    
        //$('#'+selector).removeClass('slideOutUp');
        window.setTimeout(function() { 
            $('#'+selector).addClass('slideOutUp');
            window.setTimeout(function() { 
                $('#'+selector+' .close').trigger("click");}, 600);
            }, delay);
}

$( document ).ready(function() { 


    tinymce.init({
      selector: "textarea",  // change this value according to your HTML
      plugins: "save",
      toolbar: "save, fontsizeselect",
      fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 20pt 24pt 26pt 28pt 30pt 32pt 36pt',
      save_onsavecallback: function () { 
        console.log('Saved');
        var text_content = this.getContent();
        $.post('content-save/', { text: text_content }, function() {console.log('contact saved')});
      }
    });

    $('#save-mail').click(function() {

        $('#save-mail').prop('disabled',true);
        var contact_mail = $('#contact-mail').val();

        $.post('/admin/contact/mail-save/', { contact_mail:contact_mail}, function() {
            succesalet('Zapisano Twój adres e-mail');
            $('#save-mail').prop('disabled',false);
        });
 
    });
    
    
});



