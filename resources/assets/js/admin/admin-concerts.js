function delete_article() {
    $('.delete-concert').click(function() {
        
        var concert_id=$(this).parent().parent().parent().attr("id");
        //console.log($(this).closest('div').attr('id'));

        var th = this;
        $.post('/admin/concerts/delete/', { id_to_remove: concert_id }).done(function( data ){ 
            $(th).parent().parent().parent().remove();
        });         
    });
}
var edit_id
function edit_article() {
    $('.edit-concert').click(function() {
        
        edit_id=$(this).parent().parent().parent().attr("id");
        console.log(edit_id);

        var location = $(this).parent().parent().parent().find('.location').html();
        var description = $(this).parent().parent().parent().find('.description').html();
        var datetime = $(this).parent().parent().parent().find('.date').html().trim();

        tinymce.editors['concert-location'].setContent(location);
        tinymce.editors['concert-descripiton'].setContent(description);
        //console.log(datetime);
        var date=datetime.split(' ')[0].split('-');
        var time=datetime.split(' ')[1].split(':');
        //console.log(date);

        $('#concert-date').val(date[2]+'-'+date[1]+'-'+date[0]);
        $('#concert-time').val(time[0]+':'+time[1]);

        $('#add-concert').hide();
        $('#edit-concert').show();
    });
}

function progress(e) { //e - akcja która się zdarza w trakcie wysyłania co kawałek
    if (e.lengthComputable) { //info czy da się policzyć wysyłanie
        var max = e.total; //ile jest cały
        var current = e.loaded; // ile już się wysłało
        var Percentage = current * 100 / max;
        console.log(Percentage);
        $(".progress-bar").attr('aria-valuenow',  Percentage.toFixed(0));
        $(".progress-bar").attr('style', "width:"+Percentage.toFixed(0)+"%");
        $("#upload-progress").text(Percentage.toFixed(0) + " %"); //ustawia ten procent jako tekst w jakimś elemencie
        if (Percentage >= 100) {}
    }
}

$( document ).ready(function() { 

    tinymce.init({
        selector: "textarea",
        toolbar: 'fontsizeselect',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        content_style: "p{margin-top: 0!important; margin-bottom: 0rem!important;}"
    });
    
    //obsługa zmiany napisu po dodaniu pliku do zapisania
    $('#poster-file').on('change', function() {
       // console.log('zmiana pliku');
        $( "#upload-poster-button" ).text($('#poster-file')[0].files[0].name);
    });


    //dodawanie pliku
    $('#add-concert').click(function() {
        
        console.log('funkcja wysylania');
        var fd = new FormData(); //tworzy nową formę na dane
        var poster_file = $('#poster-file')[0].files[0]; //wybrany plik
        fd.append('poster', poster_file); //dodaję plik do formy 'x'-nazwa, y - wartosc:liczba, text, plik itd

        //lokalizacja
        var concert_location = tinymce.editors['concert-location'].getContent();
        fd.append('concert_location', concert_location); 

        //program
        var concert_descripiton = tinymce.editors['concert-descripiton'].getContent();
        fd.append('concert_descripiton', concert_descripiton); 

        //data
        var concert_date = $('#concert-date').val();
        fd.append('concert_date', concert_date); 
        //godzina
        var concert_time = $('#concert-time').val();
        fd.append('concert_time', concert_time); 
        
        

        if (concert_location=='' || concert_descripiton=='' || concert_date=='' || concert_time==''){
            alert("Wpisz lokalizację i treść");
            return;
        }


        $( "#add-concert" ).prop( "disabled",true );
        $( "#concert-progress-bar" ).removeClass( "hide" );
        $.ajax({ //wysyłam plik
                    url: "save/", //adres pod który chcę go wysłać
                    type: "POST", //metoda którą chcę go wysłać
                    data: fd, //dane które chcę wysłać czyli nasza forma z danymi
                    enctype: 'multipart/form-data', //typ danych
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                    xhr: function() { //nie ważne, tutaj można dorobić śledzenie progresu wrzucania
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                }).done(function( data ){ 
                //funkcja wykonywana po zakeńczeniu wysyłania
                
                
                $('#poster-file').val('');
                tinymce.editors['concert-location'].setContent('');
                tinymce.editors['concert-descripiton'].setContent('');
                $('#concert-date').val('');
                $('#concert-time').val('');

                $( "#upload-poster-button" ).html('<i class="fa fa-paperclip"> Dodaj plakat </i>');

                $('#admin-concerts-table').find('tbody').append(`
                
                
                   <tr id="${data.new_id}">
                        <td>
                        ${concert_date} ${concert_time}
                        </td>

                        <td>
                        ${concert_location}
                        </td>

                        <td>
                        ${concert_descripiton}
                        </td>

                        <td>
                        <img class="mini-img" src="/storage/concerts/${data.new_id}.jpg"/>
                        </td>
                        
                        <td>
                        <button class="btn btn-secondary delete-concert" name="delete-concert-button"><i class="fa fa-times"></i></button>
                        </td>
                   </tr>
              
                
                `);
                
                $( "#concert-progress-bar" ).addClass( "hide" );
                $( "#add-concert" ).prop( "disabled",false);
                delete_article();
                edit_article();
                console.log(data);


                });
    });



    $('#edit-concert').click(function() {
        
        console.log('funkcja wysylania');
        var fd = new FormData(); //tworzy nową formę na dane
        var poster_file = $('#poster-file')[0].files[0]; //wybrany plik
        if (typeof poster_file !== 'undefined') {
            fd.append('poster', poster_file); //dodaję plik do formy 'x'-nazwa, y - wartosc:liczba, text, plik itd
        }
        //lokalizacja
        var concert_location = tinymce.editors['concert-location'].getContent();
        fd.append('concert_location', concert_location); 

        //program
        var concert_descripiton = tinymce.editors['concert-descripiton'].getContent();
        fd.append('concert_descripiton', concert_descripiton); 

        //data
        var concert_date = $('#concert-date').val();
        fd.append('concert_date', concert_date); 
        //godzina
        var concert_time = $('#concert-time').val();
        fd.append('concert_time', concert_time); 
        
        fd.append('concert_id', edit_id); 

        if (concert_location=='' || concert_descripiton=='' || concert_date=='' || concert_time==''){
            alert("Wpisz lokalizację i treść");
            return;
        }


        $( "#edit-concert" ).prop( "disabled",true );
        $( "#concert-progress-bar" ).removeClass( "hide" );
        $.ajax({ //wysyłam plik
                    url: "edit/", //adres pod który chcę go wysłać
                    type: "POST", //metoda którą chcę go wysłać
                    data: fd, //dane które chcę wysłać czyli nasza forma z danymi
                    enctype: 'multipart/form-data', //typ danych
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                    xhr: function() { //nie ważne, tutaj można dorobić śledzenie progresu wrzucania
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                }).done(function( data ){ 
                //funkcja wykonywana po zakeńczeniu wysyłania
                //console.log(edit_id);
                concert_date=concert_date.split('-');
                $('#'+edit_id).find('.date').html(concert_date[2]+'-'+concert_date[1]+'-'+concert_date[0]+' '+concert_time);
                $('#'+edit_id).find('.location').html(concert_location);
                $('#'+edit_id).find('.description').html(concert_descripiton);
                
                $('#'+edit_id).find('.image > img').attr('src', $('#'+edit_id).find('.image > img').attr('src')+'?rand='+Date.now());

                $('#poster-file').val('');
                tinymce.editors['concert-location'].setContent('');
                tinymce.editors['concert-descripiton'].setContent('');
                $('#concert-date').val('');
                $('#concert-time').val('');

                $( "#upload-poster-button" ).html('<i class="fa fa-paperclip"> Dodaj plakat </i>');

                
                
                $( "#concert-progress-bar" ).addClass( "hide" );
                $( "#edit-concert" ).prop( "disabled",false);
                //delete_article();
                //edit_article();
                //console.log(data);
                $('#add-concert').show();
                $('#edit-concert').hide();


                });
    });

    delete_article();
    edit_article();
});
    

    