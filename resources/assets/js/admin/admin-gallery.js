function delete_photo() {
    $('.delete-photo').click(function () {

        var photo_id = ($(this).parent().parent().attr("id"));
        //console.log($(this).closest('div').attr('id'));

        var th = this;
        $.post('/admin/gallery/delete/', { id_to_remove: photo_id }).done(function (data) {
            $(th).parent().parent().remove();
        });
    });
}

function progress(e) { //e - akcja która się zdarza w trakcie wysyłania co kawałek
    if (e.lengthComputable) { //info czy da się policzyć wysyłanie
        var max = e.total; //ile jest cały
        var current = e.loaded; // ile już się wysłało
        var Percentage = current * 100 / max;
        console.log(Percentage);
        $(".progress-bar").attr('aria-valuenow', Percentage.toFixed(0));
        $(".progress-bar").attr('style', "width:" + Percentage.toFixed(0) + "%");
        $("#upload-progress").text(Percentage.toFixed(0) + " %"); //ustawia ten procent jako tekst w jakimś elemencie
        if (Percentage >= 100) { }
    }
}

var cropper;

$(document).ready(function () {

    //obsługa zmiany napisu po dodaniu pliku do zapisania
    $('#photo-file').on('change', function () {
        $(".cropper-container").removeClass("hide");
        $("#photo-cropper").removeClass("hide");
        // console.log('zmiana pliku');
        $("#upload-photo-button").text($('#photo-file')[0].files[0].name);

        var reader = new FileReader();

        reader.onload = function (e) {
            $('#photo-cropper').attr('src', e.target.result);
            var image = document.getElementById('photo-cropper');
            cropper = new Cropper(image, {
                aspectRatio: 16 / 9,
                crop: function (e) {
                    console.log(e.detail.x);
                    console.log(e.detail.y);
                    console.log(e.detail.width);
                    console.log(e.detail.height);
                    console.log(e.detail.rotate);
                    console.log(e.detail.scaleX);
                    console.log(e.detail.scaleY);
                }
            });
        }
        reader.readAsDataURL($('#photo-file')[0].files[0]);



    });

    //sortowanie
    $("#admin-photos-table").find('tbody').sortable({
        items: "> tr",
        appendTo: "parent",
        start: function() {
           
         },
        stop: function( event ) {
            var sort = [];
            $('#admin-photos-table').find('tbody').find('tr').each(function(index, val) {
                var sorted_id = $(this).attr('id');
                sort.push(sorted_id);
            })
            $.post('sort-photos/', { sorted_ids: sort}, function() {console.log('photos sorted')});
        },
        update: function() {
           
         },
    });


    //dodawanie pliku
    $('#add-photo').click(function () {

        console.log('funkcja wysylania');
        var fd = new FormData(); //tworzy nową formę na dane

        var photo_file = $('#photo-file')[0].files[0]; //wybrany plik
        fd.append('photo', photo_file); //dodaję plik do formy 'x'-nazwa, y - wartosc:liczba, text, plik itd

        var photo_title = $('#photo-title').val();
        fd.append('photo_title', photo_title);

        if (photo_title == '' || photo_file == null) {
            alert("Wpisz tytuł i treść");
            return;
        }

        $("#add-photo").prop("disabled", true);
        $("#photo-progress-bar").removeClass("hide");


        cropper.getCroppedCanvas().toBlob(function (photo_thumb) {
            fd.append('thumb', photo_thumb);
            /*tutaj cała reszta wysyłania*/
            $.ajax({ //wysyłam plik
                url: "save/", //adres pod który chcę go wysłać
                type: "POST", //metoda którą chcę go wysłać
                data: fd, //dane które chcę wysłać czyli nasza forma z danymi
                enctype: 'multipart/form-data', //typ danych
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                xhr: function () { //nie ważne, tutaj można dorobić śledzenie progresu wrzucania
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', progress, false);
                    }
                    return myXhr;
                },
            }).done(function (data) {
                //funkcja wykonywana po zakeńczeniu wysyłania

                $('#photo-title').val('');
                $('#photo-file').val('');
                $("#upload-photo-button").html('<i class="fa fa-paperclip"></i>');

                $('#admin-photos-table').find('tbody').append(`
                
                
                   <tr id="${data.new_id}">
                        <td>
                        ${photo_title} &nbsp;&nbsp;
                        </td>

                        <td>
                        <img class="mini-img" src="/storage/gallery/${data.new_id}.jpg"/>
                        </td>
                        
                        <td>
                        <button class="btn btn-secondary delete-photo" name="delete-photo-button"><i class="fa fa-times"></i></button>
                        </td>
                   </tr>
              
                
                `);

                $("#photo-progress-bar").addClass("hide");
                $("#photo-cropper").addClass("hide");
                $(".cropper-container").addClass("hide");
                $("#add-photo").prop("disabled", false);
                $('.new_photo' + data.new_id).mediaelementplayer();
                delete_photo();
                console.log(data);


            });
        });

    });


    delete_photo();
});


