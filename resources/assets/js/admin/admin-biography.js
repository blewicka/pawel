$( document ).ready(function() { 


tinymce.init({
  selector: "textarea",  // change this value according to your HTML
  plugins: "save, nonbreaking, visualchars",
  toolbar: "save | fontsizeselect | nonbreaking | indent | mybutton | visualchars",
  fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 20pt 24pt 26pt 28pt 30pt 32pt 36pt',
  content_style: "p{margin-top: 0!important; margin-bottom: 0rem!important;} .tindent { text-indent: 40px; } .mce-nbsp, .mce-shy { background: #AAA;}",
  formats: {
    tindent_format:{selector:'p', classes:'tindent'},
  },
  save_onsavecallback: function () { 
    console.log('Saved');
    var text_content = this.getContent();
    $.post('save/', { text: text_content }, function() {console.log('biography saved')});
  },
  setup : function(ed) {
    ed.on('keydown', function(event) {
      console.debug('Key up event: ' + event.keyCode);
      if (event.keyCode == 9){ // tab pressed
        ed.execCommand('mceInsertContent', false, '&nbsp;&nbsp;'); // inserts tab
        event.preventDefault();
        event.stopPropagation();
        return false;
      }
    });
    ed.addButton('mybutton', {
      text: "Indent",
      onclick:function() {
        ed.execCommand('mceToggleFormat', false, 'tindent_format');
      },
      /*onpostrender:function() {
          var btn = this;
          ed.formatter.formatChanged('tindent_format', function(state) {
              btn.active(state);
          });
      }*/
    });
  }
});


});
