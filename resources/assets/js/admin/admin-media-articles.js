function delete_article() {
    $('.delete-article').click(function() {
        
        var article_id=($(this).parent().parent().attr("id"));
        //console.log($(this).closest('div').attr('id'));

        var th = this;
        $.post('/admin/media/articles/delete/', { id_to_remove: article_id }).done(function( data ){ 
            $(th).parent().parent().remove();
        });         
    });
}

function progress(e) { //e - akcja która się zdarza w trakcie wysyłania co kawałek
    if (e.lengthComputable) { //info czy da się policzyć wysyłanie
        var max = e.total; //ile jest cały
        var current = e.loaded; // ile już się wysłało
        var Percentage = current * 100 / max;
        console.log(Percentage);
        $(".progress-bar").attr('aria-valuenow',  Percentage.toFixed(0));
        $(".progress-bar").attr('style', "width:"+Percentage.toFixed(0)+"%");
        $("#upload-progress").text(Percentage.toFixed(0) + " %"); //ustawia ten procent jako tekst w jakimś elemencie
        if (Percentage >= 100) {}
    }
}




$( document ).ready(function() { 

    
    tinymce.init({
        selector: "textarea",
        toolbar: 'fontsizeselect',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        content_style: "p{margin-top: 0!important; margin-bottom: 0rem!important;}"
    });


    //sortowanie
    $("#admin-articles-table").find('tbody').sortable({
        items: "> tr",
        appendTo: "parent",
        start: function() {
           
         },
        stop: function( event ) {
            var sort = [];
            $('#admin-articles-table').find('tbody').find('tr').each(function(index, val) {
                var sorted_id = $(this).attr('id');
                sort.push(sorted_id);
            })
            $.post('sort-articles/', { sorted_ids: sort}, function() {console.log('articles sorted')});
        },
        update: function() {
           
         },
    });

    //obsługa zmiany napisu po dodaniu pliku do zapisania
    $('#article-file').on('change', function() {
       // console.log('zmiana pliku');
        $( "#upload-article-button" ).text($('#article-file')[0].files[0].name);
    });


    //dodawanie pliku
    $('#add-article').click(function() {
        
        console.log('funkcja wysylania');
        var fd = new FormData(); //tworzy nową formę na dane
        var article_file = $('#article-file')[0].files[0]; //wybrany plik
        fd.append('article', article_file); //dodaję plik do formy 'x'-nazwa, y - wartosc:liczba, text, plik itd

        //var article_title = $('#article-title').val();title_content
        var article_title = tinymce.activeEditor.getContent();
        fd.append('article_title', article_title); 
        
        

        if (article_title=='' || article_file==null){
            alert("Wpisz tytuł i treść");
            return;
        }


        $( "#add-article" ).prop( "disabled",true );
        $( "#article-progress-bar" ).removeClass( "hide" );
        $.ajax({ //wysyłam plik
                    url: "save/", //adres pod który chcę go wysłać
                    type: "POST", //metoda którą chcę go wysłać
                    data: fd, //dane które chcę wysłać czyli nasza forma z danymi
                    enctype: 'multipart/form-data', //typ danych
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                    xhr: function() { //nie ważne, tutaj można dorobić śledzenie progresu wrzucania
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                }).done(function( data ){ 
                //funkcja wykonywana po zakeńczeniu wysyłania
                
                tinymce.activeEditor.setContent('');
                $('#article-file').val('');
                $( "#upload-article-button" ).html('<i class="fa fa-paperclip"></i>');

                $('#admin-articles-table').find('tbody').append(`
                
                
                   <tr id="${data.new_id}">
                        <td>
                        ${article_title} &nbsp;&nbsp;
                        </td>

                        <td>
                        <img class="mini-img" src="/storage/media/articles/${data.new_id}.jpg"/>
                        </td>
                        
                        <td>
                        <button class="btn btn-secondary delete-article" name="delete-article-button"><i class="fa fa-times"></i></button>
                        </td>
                   </tr>
              
                
                `);
                
                $( "#article-progress-bar" ).addClass( "hide" );
                $( "#add-article" ).prop( "disabled",false);
                $('.new_article'+data.new_id).mediaelementplayer();
                delete_article();
                console.log(data);


                });
    });

    

    delete_article();
  
});
    

    