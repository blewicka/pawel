var open_overlay;
///storage/gallery/10.jpg
var gallery_current;
var article_current;

$(window).on('load', function() { 
    if (window.innerWidth>992){
        var offset = window.innerHeight - $('#mainNav').offset().top-100;
        if (offset>0){
            $('#biography').css('margin-top', offset+'px');
        }
    }
    else{
        $('#biography').css('margin-top', '0px');
    }
});

$(document).ready(function () {

    if (window.innerWidth>992){
        var offset = window.innerHeight - $('#mainNav').offset().top-100;
        if (offset>0){
            $('#biography').css('margin-top', offset+'px');
        }
    }
    else{
        $('#biography').css('margin-top', '0px');
    }

    
    $( window ).resize(function() {
        if (window.innerWidth>992){
            var offset = window.innerHeight - $('#mainNav').offset().top-100;
            if (offset>0){
                $('#biography').css('margin-top', offset+'px');
            }
        }
        else{
            $('#biography').css('margin-top', '0px');
        }
    });
   

    $('.openArticle').click(function () {
        document.onkeydown=articleKey;
        article_current = $(this).data('id');
        
        $('#article').removeClass('hide').find('.ovelay-arrow').removeClass('hide');
        $('#article').find('.article-image').css('background-image', 'url(/storage/media/articles/' + article_current + '-blur.jpg)');
        $('#article').find('.article-image').attr('src', '/storage/media/articles/'+article_current+'.jpg');

        setTimeout(function () {
            //$('#article').css('width', '100%');
            $('#article').addClass('open');
            $("body").addClass("noscrool");
;
        }, 20);
    });

    $('.openPhoto').click(function () {
        gallery_current = $(this).data('id');
        document.onkeydown = galleryKey;
        //open_overlay = "full-photo";
        $('#full-photo').removeClass('hide').find('.ovelay-arrow').removeClass('hide');
        $('#full-photo').find('.gallery-photo').css('background-image', 'url(/storage/gallery/' + gallery_current + '-blur.jpg)');
        $('#full-photo').find('.gallery-photo').attr('src', '/storage/gallery/'+gallery_current+'.jpg');

        setTimeout(function () {
            //$('#full-photo').css('width', '100%');
            $('#full-photo').addClass('open');
            $("body").addClass("noscrool");
;
        }, 20);
    });

    $('.closebtn-gallery').click(function () {
        //$('#full-photo').width("0%");
        $('#full-photo').removeClass('open');
        $('#full-photo').find('.gallery-photo').attr('src', '#');
        $('#full-photo').find('.ovelay-arrow').addClass('hide');
        $("body").removeClass("noscrool");
        document.onkeydown = null;
    });

    $('.closebtn-article').click(function () {
        //$('#article').width("0%");
        $('#article').removeClass('open');
        $('#article').find('.article-image').attr('src', '#');
        $('#article').find('.ovelay-arrow').addClass('hide');
        $("body").removeClass("noscrool");
        document.onkeydown = null;
    });



   
    $('.gallery-next').click(function () {
        
        gallery_current=$('.openPhoto[data-id='+gallery_current+']').parent().next().find('.openPhoto').data('id');
        if (typeof gallery_current === 'undefined') {
            $('.closebtn-gallery').click();
        } else {
            $('#full-photo').find('.gallery-photo').css('background-image', 'url(/storage/gallery/' + gallery_current + '-blur.jpg)');
            $('#full-photo').find('.gallery-photo').attr('src', '/storage/gallery/'+gallery_current+'.jpg');
        }

    });

    $('.gallery-prev').click(function () {
       
        gallery_current=$('.openPhoto[data-id='+gallery_current+']').parent().prev().find('.openPhoto').data('id');
        if (typeof gallery_current === 'undefined') {
            $('.closebtn-gallery').click();
        } else {
            $('#full-photo').find('.gallery-photo').attr('src', '/storage/gallery/'+gallery_current+'.jpg');
            $('#full-photo').find('.gallery-photo').css('background-image', 'url(/storage/gallery/' + gallery_current + '-blur.jpg)');
        }

    });


    $('.article-next').click(function () {
        
        article_current=$('.openArticle[data-id='+article_current+']').next().next().data('id');
        if (typeof article_current === 'undefined') {
            $('.closebtn-article').click();
        } else {
            $('#article').find('.article-image').css('background-image', 'url(/storage/media/articles/' + article_current + '-blur.jpg)');
            $('#article').find('.article-image').attr('src', '/storage/media/articles/'+article_current+'.jpg');
        }

    });

    $('.article-prev').click(function () {
       
        article_current=$('.openArticle[data-id='+article_current+']').prev().prev().data('id');
        if (typeof article_current === 'undefined') {
            $('.closebtn-article').click();
        } else {
            $('#article').find('.article-image').css('background-image', 'url(/storage/media/articles/' + article_current + '-blur.jpg)');
            $('#article').find('.article-image').attr('src', '/storage/media/articles/'+article_current+'.jpg');
        }

    });

    function articleKey(e){
        e = e || window.event;
        console.log(e.keyCode);
        if (open) {
            
            if (e.keyCode == '37') {
            // left arrow
                $('.article-prev').click();
            }
            else if (e.keyCode == '39') {
            // right arrow
                $('.article-next').click();
            } 
            else if (e.keyCode == '27') {
                $('.overlay').find('.closebtn').click();
            }
        
        }

    }

    function galleryKey(e){
        e = e || window.event;
        console.log(e.keyCode);
        if (open) {
            
            if (e.keyCode == '37') {
            // left arrow
                $('.gallery-prev').click();
            }
            else if (e.keyCode == '39') {
            // right arrow
                $('.gallery-next').click();
            } 
            else if (e.keyCode == '27') {
                $('.overlay').find('.closebtn').click();
            }
        
        }
    }



    $('.concert-year:not(.all-concerts)').click(function () {

        $('#up-arrow').addClass('hide');
        var curr_year = $(this).data('year');

        //chowamy dany rok koncertów
        if( $(this).hasClass('active-year')){
            
            $('#up-arrow').addClass('hide');

            if ($('.concert:visible').length > 3) {
                $('.concert:visible:nth-child(n+4)').addClass("animated fadeOutUp");

                $('.concert:visible').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                    $('.concert:visible:nth-child(n+4)').addClass('hide').removeClass('fadeOutUp');
                });

                $('html, body').animate({
                    scrollTop: $("#concerts").offset().top
                }, 1000); 
            }

        }
        //otwieramy dany rok koncertów
        else{
            $('.concert-year').removeClass('active-year');
            $('.all-concerts').removeClass('active-year');
            // $('#' + curr_year).addClass('active-year');
            $(this).addClass('active-year');

            //jeśli są wyświetlone
            if ($('.concert:visible').length != 0) {
                $('.concert:visible').addClass('animated fadeOutLeft');

                $('.concert:visible').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {

                    $('.concert:visible').addClass('hide').removeClass('fadeOutLeft');

                    $("." + curr_year).addClass("animated fadeInRight");
                    $("." + curr_year).removeClass("hide");

                    setTimeout(function () {
                        $("." + curr_year).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                            $("." + curr_year).removeClass("animated fadeInRight");

                            if ($('.concert:visible').length > 3) {
                                $('#up-arrow').removeClass('hide');
                            } else {
                                $('#up-arrow').addClass('hide');
                            }

                        })
                    }, 20);
                    

                });
            }
            //jeśli nie ma wyświetlonych
            else {
                $("." + curr_year).addClass("animated fadeInRight");
                $("." + curr_year).removeClass("hide");

                setTimeout(function () {
                    $("." + curr_year).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                        $("." + curr_year).removeClass("animated fadeInRight");

                        if ($('.concert:visible').length > 3) {
                                $('#up-arrow').removeClass('hide');
                        } else {
                            $('#up-arrow').addClass('hide');
                        }

                    })
                }, 20);
                
            }
        }

    });

    $('.all-concerts').click(function () {
        $('#up-arrow').addClass('hide');
        //dla aktywnego wszystkie koncerty TU NIE DZIAŁA
        console.log(this);
        console.log($(this).hasClass('active-year'));
        if( $(this).hasClass('active-year')){

            $('#up-arrow').addClass('hide');

            $('.concert:visible:nth-child(n+4)').addClass("animationed fadeOutUp");

            $('.concert:visible').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                $('.concert:visible:nth-child(n+4)').addClass('hide').removeClass('fadeOutUp');
            });

            $('html, body').animate({
                scrollTop: $("#concerts").offset().top
            }, 1000); 
            
        }
        //dla nieaktywnego wszystkiego koncerty, czyli otwieramy je
        else{
            $('.concert-year').removeClass('active-year');
            
            $(this).addClass('active-year');

            
            //jeśli są wyświetlone
            if ($('.concert:visible').length != 0) {
                $('.concert:visible').addClass('animated fadeOutLeft');

                $('.concert:visible').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                    $('.concert').addClass('hide').removeClass('fadeOutLeft');

                    $('.concert').addClass('animated fadeInRight');
                    $('.concert').removeClass('hide');
                    setTimeout(function () {
                        $('.concert').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                            $('.concert').removeClass("animated fadeInRight");
                            
                            if ($('.concert:visible').length > 3) {
                                $('#up-arrow').removeClass('hide');
                            } else {
                                $('#up-arrow').addClass('hide');
                            }

                        })
                    }, 20);
                });
                
            }
            //jeśli nie ma wyświetlonych
            else{
                $('.concert').addClass('animated fadeInRight');
                $('.concert').removeClass('hide');

                setTimeout(function () {
                    $('.concert').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                        $('.concert').removeClass("animated fadeInRight");

                        if ($('.concert:visible').length > 3) {
                            $('#up-arrow').removeClass('hide');
                        } else {
                            $('#up-arrow').addClass('hide');
                        }

                    })
                }, 20);

                
            }
        }
    });

    $('#up-arrow').click(function () {
        $('#up-arrow').addClass('hide');


        $('.concert:visible:nth-child(n+4)').addClass("animationed fadeOutUp");

        $('.concert:visible').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
            $('.concert:visible:nth-child(n+4)').addClass('hide').removeClass('fadeOutUp');
        });

        $('html, body').animate({
            scrollTop: $("#concerts").offset().top
        }, 1000);



        // $('.concert-year').removeClass('active-year');
        // $('.concert:visible').addClass('animated fadeOutUp');
        // $('.concert:visible').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
        //     $('.concert').addClass('hide').removeClass('fadeOutUp');
            
        //     setTimeout(function () {
               
        //         $('.concert:visible:nth-child(n+3)').addClass("animationed fadeInUp");
        //         $('.concert:nth-child(-n+3)').removeClass("hide");

        //         setTimeout(function () {
        //             $('.concert').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
        //                 $('.concert:nth-child(-n+3)').removeClass("animationed fadeInUp");
        //             })
        //         }, 20);
         
        //     }, 20);

        // });

        // $('html, body').animate({
        //     scrollTop: $("#concerts").offset().top
        // }, 1000);
        
    });


    $('body').scrollspy({
        target: '#mainNav',
        offset: 350
    });

    $('#send-msg-button').click(function () {

  

        var contact_name = $('#contact-name').val();
        var contact_email = $('#contact-email').val();
        var contact_phone = $('#contact-phone').val();
        var contact_message = $('#msg-content').val();


        
        $.post('send-mail/', { contact_name: contact_name, contact_email:contact_email, contact_phone:contact_phone, contact_message:contact_message}, function() {
            $('#contact-name').val('');
            $('#contact-email').val('');
            $('#contact-phone').val('');
            $('#msg-content').val('');

            $('#contact-form').addClass('animated zoomOut');

            $('#contact-form').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
                $('#contact-form').addClass('hide');
                $('#message-sent').addClass('animated zoomIn').removeClass("hide");
            });

            
        });

    });

    document.documentElement.classList.remove("no-js");
    document.documentElement.classList.add("js");


    $('body').click(function(event) {
        if (event.target.id != 'navbar-button' && $('#navbarSupportedContent').hasClass('show')) {
            //$('#navbarSupportedContent').removeClass('collapse').addClass('collapsing').animate({height:'toggle'},20000);; 
            $('#navbar-button').click();
        }
    });
});



